package uk.ac.liv.rvelaz.main;

//import uk.ac.liv.rvelaz.

import org.apache.xbean.spring.context.ClassPathXmlApplicationContext;
import uk.ac.liv.rvelaz.service.RequestFriends;
import uk.ac.liv.rvelaz.service.TwitterStreamSimulator;

/**
 * User: raul
 * Date: 21/11/2012
 * Time: 13:33
 * <p/>
 * options:
 * getData
 * getData csv-file-with-research-participants-data.csv
 * stream
 */
public class Main {

    public static void main(String args[]) {

        //Load spring context. It will load all the beans and inject where appropriate using DI
        ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("application-context.xml");

        //Load spring bean with name requestFriends
        RequestFriends requestFriends = (RequestFriends) ctx.getBean("requestFriends");
        //Load spring bean with name twitterStreamSimulator
        TwitterStreamSimulator twitterStreamSimulator = (TwitterStreamSimulator)ctx.getBean("twitterStreamSimulator");


        if (args[0].equals("data")) {
            if (args.length == 2) {
                requestFriends.requestFriendsForParticipants(args[1]);
            } else if (args.length == 1) {
                requestFriends.requestFriendsForParticipants();
            } else {
                System.out.println("Option non valid");
                System.exit(-1);
            }
        } else if (args[0].equals("stream")) {
            twitterStreamSimulator.simulateStream();
        } else {
            System.out.println("Option non valid");
            System.exit(-1);
        }
    }

}
