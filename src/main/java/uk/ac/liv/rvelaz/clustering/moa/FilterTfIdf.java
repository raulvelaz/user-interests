/*
 *    FilterTfIdf.java
 *    Copyright (C) 2011 University of Waikato, Hamilton, New Zealand
 *    @author Kenneth Gibson (kjjg1@waikato.ac.nz)
 *
 *    This program is free software; you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation; either version 2 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program; if not, write to the Free Software
 *    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
package uk.ac.liv.rvelaz.clustering.moa;

import moa.core.InstancesHeader;
import org.apache.lucene.analysis.TokenStream;
import uk.ac.liv.rvelaz.common.utility.StringUtility;
import uk.ac.liv.rvelaz.infrastructure.analyzer.PorterStemStopAnalyser;
import weka.core.Attribute;
import weka.core.DenseInstance;
import weka.core.Instance;
import weka.core.SparseInstance;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.*;

import uk.ac.liv.rvelaz.clustering.moa.sketch.*;

/**
 * The class for filtering a Tweet using Tf-Idf weightings
 *
 * @author Kenneth Gibson (kjjg1@waikato.ac.nz)
 *
 * Modified by Raul Velaz
 */

public class FilterTfIdf {

    protected Sketch frequentItemMiner;

    private LinkedHashMap<String, Double> tokensFreq;

    private PorterStemStopAnalyser analyser;

    protected double numOfDocs = 0;    //Number of documents in total.

    public FilterTfIdf(Sketch sketch, int numAttributes) {
        this.frequentItemMiner = sketch;
        this.tokensFreq = new LinkedHashMap<String, Double>();
        analyser = new PorterStemStopAnalyser();
    }

    /**
     * Takes a String, filters it, and calculates the tf-idf values
     * of each token in the string.
     *
     * @param s - the String to be filtered
     * @return the filtered Instance object
     */
    public Instance filter(String s, InstancesHeader header, int numAttributes) throws IOException {

        this.numOfDocs++;    //New document, count it.

        String message = analyseText(s);

        String[] tokens = message.split(" ");    //Get the individual tokens.
        double docSize = tokens.length;         //Number of tokens in the document.

        //The tokens and frequency in this specific document.
        Map<String, Integer> tokensInDoc = new HashMap<String, Integer>();

        for (String token : tokens) {
            //For each token in the document
            if (!token.equals(" ") && !token.equals("")) {
                Integer freq = tokensInDoc.get(token.toLowerCase()); //Compute freq for each token
                tokensInDoc.put(token.toLowerCase(), (freq == null) ? 1 : freq + 1);
            }
        }

        for (Map.Entry<String, Integer> e : tokensInDoc.entrySet()) { //For each token in the document
            int oldAttIndex = frequentItemMiner.getAttIndex(e.getKey());
            frequentItemMiner.addToken(e.getKey(), e.getValue());

            int newAttIndex = frequentItemMiner.getAttIndex(e.getKey());
            if (oldAttIndex == -1 && header.numAttributes() <= numAttributes) {
                // Add a new attribute since it was not there
                if (newAttIndex + 1 > header.numAttributes() - 1) {
                    // Add a new attribute
                    Attribute newAtt = new Attribute(e.getKey());

                    header.insertAttributeAt(newAtt, newAttIndex + 1);

                } else {
                    // Change the name of the attribute
                    header.renameAttribute(newAttIndex + 1, e.getKey());
                }
            }
        }

        frequentItemMiner.addDoc(docSize);
        // Create an sparse instance
        for (Map.Entry<String, Integer> e : tokensInDoc.entrySet()) { //For each token in the document
            String token = e.getKey();
            double numInDoc = e.getValue();                 //Number of occurrences of a token in the specific document.
            double docFreq = frequentItemMiner.getCount(token);        //Number of documents that the token appears in.
            double tf = numInDoc / docSize;                             //Term frequency.
            double idf = Math.log10(this.numOfDocs / (docFreq + 1));     //Inverse document frequency.
            if(!tokensFreq.containsValue(token)){
                if(tokensFreq.keySet().size() < (numAttributes -1))
                    tokensFreq.put(token, (tf * idf));
            }
            else{
                tokensFreq.put(token, (tf * idf));
            }

        }
        double [] attValues = new double[numAttributes];
        //class attribute
        attValues[0] = 0.0;
        LinkedList<String> keys = new LinkedList<String>(tokensFreq.keySet());
        for(int i = 0; i < tokens.length; i++){
            if(tokensFreq.containsKey(tokens[i])){
                int index = keys.indexOf(tokens[i]);
                attValues[index + 1] = tokensFreq.get(tokens[i]);
            }
        }

        Instance inst = new DenseInstance(1.0, attValues);
        inst.setDataset(header);

        inst.setClassValue("cluster");

        return inst;

    }

    public void printSketch() {
        this.frequentItemMiner.showNodes();
    }

    public double getFreqWord(String word) {
        return frequentItemMiner.getFreqWord(word);
    }


    /**
     * Filter tweets removing unwanted words and symbols
     * @param text
     * @return
     * @throws IOException
     */
    public String analyseText(String text) throws IOException {
		Reader reader = new StringReader(text);
		TokenStream tokenStream = analyser.tokenStream(null, reader);
		StringBuilder filteredText = new StringBuilder();
		while (true) {
			if (!tokenStream.incrementToken())
				break;
            String filtered = StringUtility.filterWordExtended(tokenStream.getAttributeImplsIterator().next()
					.toString());
            filteredText.append(filtered);
			filteredText.append(" ");
		}

		return filteredText.toString();
	}

}
