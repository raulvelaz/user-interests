package uk.ac.liv.rvelaz.clustering;

import uk.ac.liv.rvelaz.domain.Experiment;
import uk.ac.liv.rvelaz.domain.Tweet;

import java.io.IOException;
import java.util.List;

/**
 * User: raul
 * Date: 07/12/2012
 * Time: 11:49
 */
public interface ICluster {
    public void cluster(Tweet tweet) throws IOException;

    public void startNewExperiment(Experiment experiment, Tweet tweet) throws IOException;

    public List<String> endExperiment(Tweet tweet) throws IOException;
}
