package uk.ac.liv.rvelaz.clustering;

import moa.cluster.Cluster;
import moa.cluster.Clustering;
import moa.clusterers.clustream.Clustream;
import moa.clusterers.clustree.ClusTree;
import moa.core.InstancesHeader;
import moa.gui.visualization.DataPoint;
import org.apache.commons.math.stat.descriptive.DescriptiveStatistics;
import org.apache.log4j.Logger;
import uk.ac.liv.rvelaz.clustering.moa.FilterTfIdf;
import uk.ac.liv.rvelaz.clustering.moa.sketch.SpaceSaving;
import uk.ac.liv.rvelaz.domain.Experiment;
import uk.ac.liv.rvelaz.domain.Tweet;
import weka.core.Attribute;
import weka.core.Instance;
import weka.core.Instances;

import java.io.IOException;
import java.util.*;

/**
 * User: raul
 * Date: 07/12/2012
 * Time: 11:51
 * <p/>
 * Class that allows the clustering process: micro and macro-clustering
 */
public class ClusteringAlgorithm implements ICluster {
    private final static Logger logger = Logger.getLogger(ClusteringAlgorithm.class);
    //Number of attributes for the instance
    private int numAtts = 3000;
    //Data structure that preserves insertion order and
    //represents the data points to be used in clustering
    private LinkedList<DataPoint> pointBuffer0 = new LinkedList<DataPoint>();
    //Data structure that represents the data points to be used in clustering (fast access)
    private ArrayList<DataPoint> pointarray0 = null;
    private int timestamp = 0;
    //Number of data samples to keep when calculating the clustering
    private int decayHorizon = 10000;

    private int macroNumber = 0;

    private Clustering microAll = new Clustering();
    //Data points used for micro clustering
    private LinkedList<DataPoint> allPoints = new LinkedList<DataPoint>();


    private Clustering macro;

    //TF-IDF calculator
    private FilterTfIdf filterTfIdf;
    private InstancesHeader instancesHeader;
    //Algorithm used for clustering
    private Clustream clustream;

    /**
     * Cluster a tweet
     *
     * @param tweet
     * @throws IOException
     */
    @Override
    public void cluster(Tweet tweet) throws IOException {
        //Increment timestamp per received tweet
        timestamp++;
        //Create an instance from TD-IDF
        Instance instance = filterTfIdf.filter(tweet.getText(), instancesHeader, numAtts);
        //Build a data point from the instance and timestamp
        DataPoint dataPoint = new DataPoint(instance, timestamp);
        pointBuffer0.add(dataPoint);
        //Keep only data samples within the decay horizon range
        if (pointBuffer0.size() > decayHorizon)
            pointBuffer0.removeFirst();

        //Data structure of faster time access that contains data to be used for clustering
        pointarray0 = new ArrayList<DataPoint>(pointBuffer0);

        //Cluster sample
        clustream.trainOnInstanceImpl(instance);

        //Produce micro-clusters every 1,000 samples
        if (instance.dataset().numAttributes() > numAtts && timestamp % 1000 == 0)
            processMicroClusters(clustream.getMicroClusteringResult(), pointarray0, instance);

    }

    /**
     * Starts a new experiment for a research participant.
     *
     * @param experiment
     * @param tweet
     * @throws IOException
     */
    @Override
    public void startNewExperiment(Experiment experiment, Tweet tweet) throws IOException {
        logger.debug("New experiment for user " + experiment.getParticipant().getScreenName());

        //Create attributes
        ArrayList<Attribute> attributes = new ArrayList<Attribute>(numAtts + 1);
        ArrayList<String> classVal = new ArrayList<String>();
        classVal.add("cluster");
        //Class attribute is necessary for MOA instances
        Attribute classAtt = new Attribute("instance_class", classVal);
        attributes.add(classAtt);

        //The number of attributes will be equal to the number of terms we are going to
        //use plus the class attribute
        Instances instances = new Instances("sparseInstances", attributes, numAtts + 1);
        instancesHeader = new InstancesHeader(instances);
        instancesHeader.setClassIndex(0);

        //Instantiate class for TF-IDF
        SpaceSaving sketch = new SpaceSaving();
        sketch.prepareForUse();
        filterTfIdf = new FilterTfIdf(sketch, numAtts);

        //Clustering algorithm initialization
        clustream = new Clustream();
        clustream.prepareForUse();
        clustream.setModelContext(instancesHeader);

        //Increase timestamp
        timestamp++;

        //Create instance
        Instance instance = filterTfIdf.filter(tweet.getText(), instancesHeader, numAtts);
        DataPoint dataPoint = new DataPoint(instance, timestamp);
        pointBuffer0.add(dataPoint);

        clustream.trainOnInstanceImpl(instance);

    }

    /**
     * End the experiment and find the suggested topics
     *
     * @param tweet
     * @return list containing the suggested topics
     * @throws IOException
     */
    @Override
    public List<String> endExperiment(Tweet tweet) throws IOException {
        //Create instance using TF-IDF
        Instance instance = filterTfIdf.filter(tweet.getText(), instancesHeader, numAtts);
        DataPoint dataPoint = new DataPoint(instance, timestamp);
        pointBuffer0.add(dataPoint);
        if (pointBuffer0.size() > decayHorizon)
            pointBuffer0.removeFirst();
        pointarray0 = new ArrayList<DataPoint>(pointBuffer0);
        clustream.setModelContext(instancesHeader);

        //Merge micro-clusters into macro-clusters
        Map<String, Double> attributeMap = processMacro(instance);

        // Get entries and sort them according to the value
        List<Map.Entry<String, Double>> entries = new ArrayList<Map.Entry<String, Double>>(attributeMap.entrySet());
        Collections.sort(entries, new Comparator<Map.Entry<String, Double>>() {
            public int compare(Map.Entry<String, Double> e1, Map.Entry<String, Double> e2) {
                return e2.getValue().compareTo(e1.getValue());
            }
        });

        //Create object to gather statistic data
        DescriptiveStatistics stats = new DescriptiveStatistics();
        // Put entries back in an ordered map.
        Map<String, Double> orderedMap = new LinkedHashMap<String, Double>();
        for (Map.Entry<String, Double> entry : entries) {
            stats.addValue(entry.getValue());
            orderedMap.put(entry.getKey(), entry.getValue());
        }

        //Calculate statistical parameters
        double mean = stats.getMean();
        double std = stats.getStandardDeviation();

        List<String> clusters = new LinkedList<String>();
        //Filter out non-significant attributes
        for (String attribute : orderedMap.keySet()) {
            if (orderedMap.get(attribute) <= Math.abs(mean + std * 2) * 5 && orderedMap.get(attribute) > mean * 5) {
                clusters.add(attribute);
            }
        }

        return clusters;

    }

    /**
     * Creates a micro-cluster from incoming data
     *
     * @param micro    result of algorithm clustering
     * @param points0  data points to consider
     * @param instance
     * @throws IOException
     */
    private void processMicroClusters(Clustering micro, ArrayList<DataPoint> points0, Instance instance) throws IOException {
        macroNumber++;
        Clustering gtClust = new Clustering(points0);
        logger.debug("Micro clustering size: " + micro.size());


        //Add resulting micro-clusters to a data structure that will
        //store them and later be used in macro-clustering calculation
//        for (Cluster cluster : micro.getClustering()) {
//            microAll.add(cluster);
//        }

        allPoints.addAll(points0);
        while (allPoints.size() > decayHorizon)
            allPoints.removeFirst();

        //Merge micro-clusters into macro-clusters
        macro = moa.clusterers.KMeans.gaussianMeans(gtClust, micro);

        //Add resulting macro-clusters to a data structure that will
        //store them and later be used in macro-clustering calculation
        for (Cluster cluster : macro.getClustering()) {
            microAll.add(cluster);
        }





    }

//    private void writeToFile(int macroNumber, String text) throws IOException {
//        String path = "/Users/raul/dev/uol/userInterest/cluster/ClusTree/61299563-XX/";
//        String fileName = path + macroNumber + "-ClusTree-2000-clustering.csv";
//
//        File file = new File(fileName);
//
//        //if file doesnt exists, then create it
//        if (!file.exists()) {
//            file.createNewFile();
//        }
//        //true = append file
//        FileWriter fileWritter = new FileWriter(file.getName(), true);
//        PrintWriter printWriter = new PrintWriter(fileWritter);
//        printWriter.println(text);
//        printWriter.close();
//
//
//    }

    /**
     * Takes an instance and returns a map of attributes and TF-IDF value
     *
     * @param instance
     * @return map of attributes and TF-IDF value
     * @throws IOException
     */
    private Map<String, Double> processMacro(Instance instance) throws IOException {
        Map<String, Double> attMap = new HashMap<String, Double>();

        Clustering gtClust = new Clustering(allPoints);
        Clustering macro = moa.clusterers.KMeans.gaussianMeans(gtClust, microAll);

        System.out.println("Macro clustering size: " + macro.size());

        List<Cluster> macroClust = macro.getClustering();
        int num = 555000;
        for (int k = 0; k < macroClust.size(); k++) {
            num++;
//            writeToFile(num, "##########################");
//            writeToFile(num, "Cluster " + k);


            double[] centre = macroClust.get(k).getCenter();
            logger.debug("Starting final macro clustering. Centre length: " + centre.length);
            for (int i = 0; i < centre.length; i++) {
                if (centre[i] != 0) {
                    attMap.put(instance.attribute(i).name(), centre[i]);
//                    writeToFile(num, instance.attribute(i).name() + "," + centre[i]);
                }
            }
        }
        return attMap;

    }
}
