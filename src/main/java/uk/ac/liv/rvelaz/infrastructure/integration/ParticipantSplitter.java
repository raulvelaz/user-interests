package uk.ac.liv.rvelaz.infrastructure.integration;

import uk.ac.liv.rvelaz.domain.Participant;

import java.util.List;

/**
 * User: raul
 * Date: 28/11/2012
 * Time: 17:41
 *
 * Splits a message containing a list of participants into a series of messages with only
 * one participant each
 *
 */
public class ParticipantSplitter {
    public List<Participant> split(List<Participant> participants){
        return participants;
    }
}
