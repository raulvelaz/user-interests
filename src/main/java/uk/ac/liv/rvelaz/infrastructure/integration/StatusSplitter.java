package uk.ac.liv.rvelaz.infrastructure.integration;

import twitter4j.Status;

import java.util.List;

/**
 * User: raul
 * Date: 28/11/2012
 * Time: 17:41
 *
 * Splits a message containing a list of Status (Twitter4j tweets) into a series of messages with only
 * one status each
 *
 */
public class StatusSplitter {
    public List<Status> split(List<Status> statuses){
        return statuses;
    }
}
