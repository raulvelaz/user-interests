package uk.ac.liv.rvelaz.infrastructure.integration;

import com.cybozu.labs.langdetect.Detector;
import com.cybozu.labs.langdetect.DetectorFactory;
import com.cybozu.labs.langdetect.LangDetectException;
import org.apache.log4j.Logger;
import twitter4j.Status;
import uk.ac.liv.rvelaz.domain.Tweet;

/**
 * User: raul
 * Date: 06/12/2012
 * Time: 11:14
 * <p/>
 * Filter that filters out Tweets that are not in English.
 * It uses the langdetect library (http://code.google.com/p/language-detection/) and some language profile information stored in
 * resources/profiles
 */
public class LanguageFilter {

    private final static Logger logger = Logger.getLogger(LanguageFilter.class);

    public LanguageFilter(String path) {
        try {
            DetectorFactory.loadProfile("/home/rvelaz/uol/profiles/");
            logger.debug("Profile loaded from: " + path);
        } catch (LangDetectException e) {
            logger.debug("LangDetectException " + e.getMessage());
        }
    }

    /**
     * Returns true if the tweet language is in English
     *
     * @param tweet
     * @return true if the tweet language is in English. False otherwise.
     * @throws LangDetectException
     */
    public boolean filter(Tweet tweet) {
        Detector detector = null;
        try {
            detector = DetectorFactory.create();

            detector.append(tweet.getText());
            String language = detector.detect();
            if (language.toLowerCase().equals("en")) {
                return true;
            }
            return false;
        } catch (LangDetectException e) {
            logger.error("LangDetectException: " + e.getMessage() + " for tweet " + tweet.getText());
            return false;
        }

    }
}
