package uk.ac.liv.rvelaz.infrastructure.repository;

import org.apache.log4j.Logger;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Order;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 *
 * Class with generic repository methods
 */

@Transactional
public abstract class GenericRepository<T> {

    // /////////////////////// INSTANCE VARIABLES ///////////////////////
    @Resource(name = "mongoTemplate")
    protected MongoTemplate mongoTemplate;

    private Class<T> clazz;

    public GenericRepository(Class<T> clazz) {
        this.clazz = clazz;
    }

    public List<T> getAll() {

        List<T> items = mongoTemplate.findAll(clazz, getCollection());
        return items;
    }

    public T get(Criteria criteria) {

        Query query = new Query(criteria);
        return mongoTemplate.findOne(query, clazz, getCollection());

    }

    public List<T> getAll(Criteria criteria) {

        Query query = new Query(criteria);
        return mongoTemplate.find(query, clazz, getCollection());

    }

    public List<T> getAllOrdered(Criteria criteria, String key, Order order) {

        Query query = new Query(criteria);
        query.sort().on(key, order);
        return mongoTemplate.find(query, clazz, getCollection());

    }

    public void add(T item) {
        mongoTemplate.insert(item, getCollection());
    }

    public abstract String getCollection();


}
