package uk.ac.liv.rvelaz.infrastructure.integration;

import org.apache.log4j.Logger;
import uk.ac.liv.rvelaz.domain.Participant;
import uk.ac.liv.rvelaz.domain.Tweet;

import java.util.List;

/**
 * User: raul
 * Date: 28/11/2012
 * Time: 17:41
 *
 * Splits a message containing a list of tweets into a series of messages with only
 * one tweet each
 *
 */
public class TweetSplitter {
    private final static Logger logger = Logger.getLogger(TweetSplitter.class);

    public List<Tweet> split(List<Tweet> tweets){
        return tweets;
    }
}
