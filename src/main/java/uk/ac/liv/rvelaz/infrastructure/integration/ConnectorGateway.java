package uk.ac.liv.rvelaz.infrastructure.integration;

import uk.ac.liv.rvelaz.domain.Participant;

/**
 * User: raul
 * Date: 21/11/2012
 * Time: 13:26
 *
 * A gateway starts the integration flow. i.e, it will transform a participant
 * retrieved from the CSV file into a Message and will send it through
 * the application components described in resources/application-context.xml
 *
 */
public interface ConnectorGateway {
    void sendParticipant(Participant participant);
}
