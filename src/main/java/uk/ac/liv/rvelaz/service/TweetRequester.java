package uk.ac.liv.rvelaz.service;

import org.apache.commons.lang.ArrayUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import twitter4j.*;
import twitter4j.auth.AccessToken;
import uk.ac.liv.rvelaz.domain.Participant;
import uk.ac.liv.rvelaz.domain.ParticipantFriend;

import java.util.ArrayList;
import java.util.List;

/**
 * User: raul
 * Date: 21/11/2012
 * Time: 16:01
 *
 * Class that manages the request to Twitter for tweets for a particular user.
 *
 */

public class TweetRequester extends TwitterRequest{

    private final static Logger logger = Logger.getLogger(TweetRequester.class);


    private final static Integer MAX_TWEETS = 200;

    /**
     * Requests Twitter the tweets (Status) for a research participant
     * @param participant
     * @return
     */
    public List<Status> requestTweets(Participant participant) {
        logger.debug("About to request tweets for participant : " + participant.getUserId());

        getRateLimitStatusAndWaitIfNecessary("[Init request tweets]");

        ResponseList<Status> statuses = null;
        List<Status> statusesTotal = new ArrayList<Status>();
        try {
            int page = 1;
            //Only 200 tweets are sent per request
            Paging paging = new Paging();
            paging.setCount(MAX_TWEETS);
            paging.setPage(page);
            statuses = twitter.getUserTimeline(participant.getUserId(), paging);

            statusesTotal.addAll(statuses);

            //If a user has more than 200 requests, the rest of data is obtained by paging
            while (statuses.size() > 150) {
                page++;
                getRateLimitStatusAndWaitIfNecessary("[Getting tweets]");
                Paging morePages = new Paging();
                morePages.setCount(MAX_TWEETS);
                morePages.setPage(page);
                statuses = twitter.getUserTimeline(participant.getUserId(), morePages);

                statusesTotal.addAll(statuses);
            }

        } catch (TwitterException e) {
            logger.error("Error: " + e.getExceptionCode() + " " + e.getMessage());
        }
        logger.debug("Retrieved " + statusesTotal.size() + " tweets for user " + participant.getUserId());
        return statusesTotal;
    }



}
