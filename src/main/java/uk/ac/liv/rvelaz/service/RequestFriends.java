package uk.ac.liv.rvelaz.service;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import uk.ac.liv.rvelaz.domain.Participant;
import uk.ac.liv.rvelaz.infrastructure.integration.ConnectorGateway;
import uk.ac.liv.rvelaz.common.to.ParticipantTO;
import uk.ac.liv.rvelaz.common.transformer.CSVTransformer;
import uk.ac.liv.rvelaz.common.transformer.ParticipantTransformer;

import java.util.List;

/**
 * User: raul
 * Date: 21/11/2012
 * Time: 13:50
 *
 * Class that takes as an input the CSV file containing all the research participants
 * and starts the integration flow to get all the Twitter context data for a participant
 * and friends.
 *
 */
public class RequestFriends {
    private final static Logger logger = Logger.getLogger(RequestFriends.class);


    @Autowired
    private CSVTransformer csvTransformer;

    @Autowired
    private ConnectorGateway connectorGateway;

    @Autowired
    private ParticipantTransformer participantTransformer;

    /**
     * Get the list of participant Transfer objects from the CSV file
     * and starts the integration flow
     * @param file CSV file containing the list of research participants
     */
    public void requestFriendsForParticipants(String file) {
        List<ParticipantTO> participantTOs = csvTransformer.getParticipants(file);
        startIntegrationFlow(participantTOs);
    }

    //Default file
    public void requestFriendsForParticipants() {
        List<ParticipantTO> participantTOs = csvTransformer.getParticipants();
        startIntegrationFlow(participantTOs);
    }

    private void startIntegrationFlow(List<ParticipantTO> participantTOs) {
        logger.debug("Total number of participants: " + participantTOs.size());

        //For each participant in the file, send a message to the integration flow
        for (ParticipantTO participantTO : participantTOs) {
            logger.debug("About to request friends for : " + participantTO.getScreenName());
            Participant participant = participantTransformer.transformTO(participantTO);
            connectorGateway.sendParticipant(participant);
        }
    }
}
