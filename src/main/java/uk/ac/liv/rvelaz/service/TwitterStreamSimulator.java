package uk.ac.liv.rvelaz.service;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import uk.ac.liv.rvelaz.domain.Experiment;
import uk.ac.liv.rvelaz.domain.Participant;
import uk.ac.liv.rvelaz.domain.repository.ExperimentRepository;
import uk.ac.liv.rvelaz.domain.repository.ParticipantRepository;
import uk.ac.liv.rvelaz.infrastructure.integration.ConnectorGateway;

import java.util.List;

/**
 * User: raul
 * Date: 07/12/2012
 * Time: 08:59
 * <p/>
 * Class that gets tweets from the DB and sends it to the integration flow so that
 * it is simulated the streaming nature of Twitter.
 */
public class TwitterStreamSimulator {
    private final static Logger logger = Logger.getLogger(TwitterStreamSimulator.class);


    @Autowired
    @Qualifier("experimentGateway")
    private ConnectorGateway connectorGateway;

    private ParticipantRepository participantRepository;

    private ExperimentRepository experimentRepository;

    public TwitterStreamSimulator(ParticipantRepository participantRepository, ExperimentRepository experimentRepository) {
        this.participantRepository = participantRepository;
        this.experimentRepository = experimentRepository;
    }

    public void simulateStream() {
        List<Participant> testParticipants = participantRepository.getTestParticipants();

        for (Participant participant : testParticipants) {
            if (!experimentRepository.hasExperiment(participant)){
                logger.debug("About send participant " + participant.getScreenName() + " to streaming flow");
                connectorGateway.sendParticipant(participant);
            }
            logger.debug("Participant " + participant.getScreenName() + " already has experiment");
        }
    }
}
