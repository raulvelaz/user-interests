package uk.ac.liv.rvelaz.service;

import org.apache.log4j.Logger;
import org.springframework.integration.Message;
import org.springframework.integration.support.MessageBuilder;
import twitter4j.Status;
import uk.ac.liv.rvelaz.domain.Participant;
import uk.ac.liv.rvelaz.domain.Tweet;
import uk.ac.liv.rvelaz.domain.repository.ParticipantRepository;
import uk.ac.liv.rvelaz.domain.repository.TweetRepository;

import java.util.ArrayList;
import java.util.List;

/**
 * User: raul
 * Date: 06/12/2012
 * Time: 12:27
 * <p/>
 * Class that allows to get all the tweets from a participant and Twitter friends
 */
public class TweetCorpusFinder {
    private final static Logger logger = Logger.getLogger(TweetCorpusFinder.class);

    private ParticipantRepository participantRepository;
    private TweetRepository tweetRepository;

    //Dependency injection using a constructor
    public TweetCorpusFinder(ParticipantRepository participantRepository, TweetRepository tweetRepository) {
        this.tweetRepository = tweetRepository;
        this.participantRepository = participantRepository;
    }

    /**
     * Gets all the tweets from the repository that belong to a research participant and his friends
     *
     * @param participant
     * @return message containing a list of all the tweets
     */
    public Message<List<Tweet>> getTweets(Participant participant) {
        logger.debug("About to get tweets for participant: " + participant.getScreenName());
        List<Tweet> statusesCorpus = new ArrayList<Tweet>();
        List<Tweet> participantStatuses = tweetRepository.findTweets(participant);
        logger.debug("Tweets retrieved: " + participantStatuses.size());

        statusesCorpus.addAll(participantStatuses);

        List<Participant> participantFriends = participantRepository.getFriendsForParticipant(participant);

        for (Participant participantFriend : participantFriends) {
            logger.debug("About to get tweets for participant: " + participantFriend.getUserId());
            List<Tweet> statuses = tweetRepository.findTweets(participantFriend);
            logger.debug("Tweets retrieved: " + statuses.size());
            logger.debug("Subtotal: " + (statuses.size() + statusesCorpus.size()));
            if(statuses.size() + statusesCorpus.size() < 100000){
                statusesCorpus.addAll(statuses);
            } else{
                break;
            }
        }
        logger.debug("Total tweets for user " + participant.getScreenName() + " " + statusesCorpus.size());

        //The payload of the message is the complete list of tweets by the user and their friends.
        //In the header the participant Twitter ID will be used to differentiate one TwitterCorpus from another.
        Message<List<Tweet>> message = MessageBuilder.withPayload(statusesCorpus)
                .setHeader("participant", participant.getUserId())
                .build();

        return message;
    }
}
