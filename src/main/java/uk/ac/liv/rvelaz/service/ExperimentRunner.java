package uk.ac.liv.rvelaz.service;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.integration.Message;
import uk.ac.liv.rvelaz.clustering.ICluster;
import uk.ac.liv.rvelaz.domain.Experiment;
import uk.ac.liv.rvelaz.domain.Participant;
import uk.ac.liv.rvelaz.domain.Tweet;
import uk.ac.liv.rvelaz.domain.repository.ExperimentRepository;
import uk.ac.liv.rvelaz.domain.repository.ParticipantRepository;

import java.io.IOException;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

/**
 * User: raul
 * Date: 07/12/2012
 * Time: 12:37
 *
 * Class that handles the management of research participant experiments
 */
public class ExperimentRunner {
    private final static Logger logger = Logger.getLogger(ExperimentRunner.class);


    @Autowired
    private ICluster clusteringAlgorithm;

    private ExperimentRepository experimentRepository;
    private ParticipantRepository participantRepository;
    private Map<Long, Experiment> experimentMap;

    private Long oldUserId;

    public ExperimentRunner(ParticipantRepository participantRepository, ExperimentRepository experimentRepository) {
        this.participantRepository = participantRepository;
        this.experimentRepository = experimentRepository;
        this.experimentMap = new Hashtable<Long, Experiment>();
    }

    /**
     * Decides to start a new experiment or continue with an existing one depending if there has been
     * a change in the research participant ID.
     * @param message
     * @throws IOException
     */
    public void startOrContinueExperiment(Message<Tweet> message) throws IOException {
        Long userId = (Long) message.getHeaders().get("participant");
        if (oldUserId == null || oldUserId != null && oldUserId.equals(userId)) {           //First time or tweet for same user
            if (!experimentMap.containsKey(userId)) {                                       //Experiment does not exist
                Participant participant = participantRepository.getParticipantById(userId);
                //Start a experiment
                logger.debug("Experiment started for participant " + participant.getScreenName() + " user ID " + oldUserId);
                Experiment experiment = startExperiment(participant, message);
                //Save the experiment in memory
                experimentMap.put(userId, experiment);

            } else {
                Experiment experiment = experimentMap.get(userId);                         //Experiment already exists
                int numberTweets = experiment.getNumberOfTweets();
                numberTweets++;
                experiment.setNumberOfTweets(numberTweets);
                experimentMap.put(userId, experiment);
                clusteringAlgorithm.cluster(message.getPayload());
                if(numberTweets%100 == 0)
                    logger.debug("Experiment continued for participant: " + experiment.getParticipant().getScreenName() + " Tweets: " + numberTweets);
            }
        } else {
            //Tweet for a different user: change of experiment
            Experiment experimentComplete = experimentMap.get(oldUserId);
            logger.debug("End experiment for participant " + oldUserId + " tweets for experiment " + experimentComplete.getNumberOfTweets());
            List<String> terms = clusteringAlgorithm.endExperiment(message.getPayload()); //End the experiment and calculate topics

            experimentComplete.setTopics(terms);
            experimentRepository.add(experimentComplete);                                   //Save experiment in DB
            logger.debug("Experiment ended and saved for participant " + oldUserId);

            //Start new experiment
            Participant participant = participantRepository.getParticipantById(userId);
            logger.debug("Experiment started for participant " + participant.getScreenName());
            if(participant != null){
                Experiment experiment = startExperiment(participant, message);
                experimentMap.put(userId, experiment);
            }
        }
        oldUserId = userId;
    }

    public Map<Long, Experiment> getExperimentMap() {
        return experimentMap;
    }

    /**
     * Starts a new clustering experiment for a research participant
     *
     * @param participant
     * @param message Twitter message
     * @return experiment
     * @throws IOException
     */
    private Experiment startExperiment(Participant participant, Message<Tweet> message) throws IOException {
        Experiment experiment = new Experiment();
        experiment.setNumberOfTweets(1);
        experiment.setParticipant(participant);
        clusteringAlgorithm.startNewExperiment(experiment, message.getPayload());

        return experiment;
    }

    public void setClusteringAlgorithm(ICluster clusteringAlgorithm) {
        this.clusteringAlgorithm = clusteringAlgorithm;
    }

    public ICluster getClusteringAlgorithm() {
        return clusteringAlgorithm;
    }
}

