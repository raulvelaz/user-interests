package uk.ac.liv.rvelaz.service;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import twitter4j.RateLimitStatus;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;

/**
 * User: raul
 * Date: 04/12/2012
 * Time: 18:08
 * <p/>
 * Base class that contains common methods for authentication and rate limit requesting to Twitter.
 */
public class TwitterRequest {
    private final static Logger logger = Logger.getLogger(TwitterRequest.class);

    private int limit = 350;

    @Autowired
    private TwitterFactory twitterFactory;

    @Value("${tokenSecret}")
    private String tokenSecret;

    @Value("${token}")
    private String token;

    @Value("${consumerKey}")
    private String consumerKey;

    @Value("${consumerSecret}")
    private String consumerSecret;

    protected Twitter twitter;

    /**
     * Authentication to Twitter using OAuth
     */
    protected void authenticate() {
        AccessToken accessToken = new AccessToken(token, tokenSecret);
        twitter = twitterFactory.getInstance();
        twitter.setOAuthConsumer(consumerKey, consumerSecret);
        twitter.setOAuthAccessToken(accessToken);
    }

    //Calculate if the API limit has been reached. If that is the case, it is necessary
    //to wait 1h
    protected synchronized RateLimitStatus getRateLimitStatusAndWaitIfNecessary(String msg) {
        limit --;

        RateLimitStatus rateLimitStatus = null;
        try {
            rateLimitStatus = twitter.getRateLimitStatus();
            int remainingHits = rateLimitStatus.getRemainingHits();
            logger.debug(msg + " Remaining hits: " + remainingHits);
            logger.debug(msg + " Remaining LIMIT: " + limit);
            int secondsToWait = rateLimitStatus.getSecondsUntilReset();
            logger.debug("Seconds to wait " + rateLimitStatus.getSecondsUntilReset());
            try {
                if (limit == 0) {

                    if (secondsToWait < 3600) {
                        secondsToWait = 3610;
                    }
                    logger.debug("Due to restricions wait: " + secondsToWait + " seconds.");
                    Thread.sleep(secondsToWait * 1000);
                    limit = 350;
                    logger.debug("Time restriction finished");

                } else {
//                    if (secondsToWait < 1000) {
//                        logger.debug("Due to restricions wait: " + secondsToWait + " seconds.");
//                        Thread.sleep((secondsToWait + 10) * 1000);
//                        logger.debug("Time restriction finished");
//                    }
                }
            } catch (InterruptedException e) {
                logger.error("InterruptedException " + e.getMessage());
            }

        } catch (TwitterException e) {
            logger.error("TwitterException " + e.getMessage());
        }
        return rateLimitStatus;
    }
}
