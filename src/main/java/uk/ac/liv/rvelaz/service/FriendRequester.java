package uk.ac.liv.rvelaz.service;

import org.apache.commons.lang.ArrayUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import twitter4j.*;
import twitter4j.auth.AccessToken;
import uk.ac.liv.rvelaz.domain.Participant;
import uk.ac.liv.rvelaz.domain.ParticipantFriend;

import java.util.ArrayList;
import java.util.List;

/**
 * User: raul
 * Date: 21/11/2012
 * Time: 16:01
 *
 * Class that allows to get all the Twitter friends list from a Participant.
 *
 */

public class FriendRequester extends TwitterRequest {

    private final static Logger logger = Logger.getLogger(FriendRequester.class);

    public List<Participant> requestFriends(Participant participant) throws TwitterException {
        //First check if the rate limit has been reached and it is necessary to wait
        getRateLimitStatusAndWaitIfNecessary("[Request friends]");
        //Get all the friends of a research participant
        Participant participantWithFriends = findTwitterFriends(participant);
        //Get Twitter IDs of the research participant and friends
        long[] allUsers = getAllUserIds(participantWithFriends);
        ResponseList<User> responseList = getUsersFromTwitter(allUsers);
        //Generate a list of domain objects containing research participant and friends
        List<Participant> participantList = addTwitterUsersToParticipantList(responseList, participantWithFriends);

        return participantList;
    }

    /**
     * Asks Twitter for the friends of a research participant and populates its friendsIds
     * attribute with their userIDs
     * @param participant
     * @return
     * @throws TwitterException
     */
    private Participant findTwitterFriends(Participant participant) throws TwitterException {
        IDs iDs = twitter.getFriendsIDs(participant.getScreenName(), -1);

        List<Long> ids = convertToList(iDs.getIDs());

        participant.setFriendsIds(ids);
        String[] screenNames = {participant.getScreenName()};
        //Request Twitter user to Twitter
        ResponseList<User> participantUser = twitter.lookupUsers(screenNames);

        participant.setUserId(participantUser.get(0).getId());

        //Request friends IDs to Twitter
        while (iDs.getNextCursor() != 0) {
            getRateLimitStatusAndWaitIfNecessary("[Find friends]");
            iDs = twitter.getFriendsIDs(participant.getScreenName(), iDs.getNextCursor());
            List<Long> friendList = convertToList(iDs.getIDs());
            participant.getFriendsIds().addAll(friendList);
        }

        return participant;
    }

    /**
     * Generates a list of participant domain objects from the data obtained from Twittter
     * @param responseList user data obtained from Twitter
     * @param participant domain object that represents the research participant
     * @return list of participants
     */
    private List<Participant> addTwitterUsersToParticipantList(ResponseList<User> responseList, Participant participant) {
        List<Participant> participantList = new ArrayList<Participant>();
        for (int i = 0; i < responseList.size(); i++) {
            if (responseList.get(i) != null) {
                int statusesCount = responseList.get(i).getStatusesCount();
                int followersCount = responseList.get(i).getFollowersCount();
                long userId = responseList.get(i).getId();

                if (i == 0) {
                    participant.setStatusesCount(statusesCount);
                    participant.setFollowersCount(followersCount);
                    participantList.add(participant);
                } else {
                    ParticipantFriend participantFriend = new ParticipantFriend();
                    participantFriend.setUserId(userId);
                    participantFriend.setFriendScreenName(participant.getScreenName());
                    participantFriend.setFollowersCount(followersCount);
                    participantFriend.setStatusesCount(statusesCount);

                    participantList.add(participantFriend);
                }
            }
        }

        return participantList;

    }

    private long[] getAllUserIds(Participant participant) {
        long[] friends = friendsAsArray(participant.getFriendsIds());

        long[] user = {participant.getUserId()};
        long[] allUsers = new long[friends.length + 1];

        System.arraycopy(user, 0, allUsers, 0, user.length);
        System.arraycopy(friends, 0, allUsers, user.length, friends.length);

        return allUsers;
    }

    private ResponseList<User> getUsersFromTwitter(long[] userIds) throws TwitterException {
        ResponseList<User> responseList = null;
        if (userIds.length <= 100) {
            responseList = twitter.lookupUsers(userIds);
        } else {
            int numberOfLookUpRequests;
            if (userIds.length / 100 == 0) {
                numberOfLookUpRequests = (userIds.length) / 100;
            } else {
                numberOfLookUpRequests = (userIds.length) / 100 + 1;
            }
            for (int i = 0; i < numberOfLookUpRequests; i++) {
                long[] subArray = ArrayUtils.subarray(userIds, i * 100, 100 * (i + 1));
                if (i == 0) {
                    responseList = twitter.lookupUsers(subArray);
                } else
                    responseList.addAll(twitter.lookupUsers(subArray));
            }
        }


        return responseList;
    }


    private List<Long> convertToList(long[] idArray) {
        List<Long> ids = new ArrayList<Long>();
        for (long id : idArray) {
            ids.add(id);
        }
        return ids;
    }

    private long[] friendsAsArray(List<Long> ids) {
        Long[] array = ids.toArray(new Long[ids.size()]);
        long[] out = new long[array.length];
        for (int i = 0; i < array.length; i++) {
            out[i] = array[i].longValue();
        }
        return out;
    }

}
