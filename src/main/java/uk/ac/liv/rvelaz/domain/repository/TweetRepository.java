package uk.ac.liv.rvelaz.domain.repository;

import org.apache.log4j.Logger;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import twitter4j.Status;
import uk.ac.liv.rvelaz.domain.Participant;
import uk.ac.liv.rvelaz.domain.Tweet;
import uk.ac.liv.rvelaz.infrastructure.repository.GenericRepository;

import java.util.List;

/**
 *
 * User: raul
 * Date: 21/11/2012
 * Time: 18:33
 * Repository class that manages research Tweets
 */
public class TweetRepository extends GenericRepository<Tweet> {
    private final static Logger logger = Logger.getLogger(TweetRepository.class);

     private String collection;

    public TweetRepository(Class<Tweet> clazz, String collection) {
        super(clazz);
        this.collection = collection;
    }

    public TweetRepository(String collection) {
        super(Tweet.class);
        this.collection = collection;
    }

    @Override
    public String getCollection() {
        return this.collection;
    }

    /**
     * Get the tweets for a research participant or a friend of a research participant
     * @param participant
     * @return
     */
    public List<Tweet> findTweets(Participant participant) {
        Criteria criteria = Criteria.where("user._id").is(participant.getUserId());
        Query query = new Query(criteria);
        return mongoTemplate.find(query, Tweet.class, collection);
    }
}
