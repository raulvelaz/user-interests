package uk.ac.liv.rvelaz.domain;

/**
 * User: raul
 * Date: 21/11/2012
 * Time: 11:56
 *
 * Class that represents a research participant friend in Twitter
 *
 */
public class ParticipantFriend extends Participant{

    public ParticipantFriend() {
        super.setControlGroup(false);
    }

    private String friendScreenName;
    private Long friendUserId;

    public String getFriendScreenName() {
        return friendScreenName;
    }

    public void setFriendScreenName(String friendScreenName) {
        this.friendScreenName = friendScreenName;
    }

    public Long getFriendUserId() {
        return friendUserId;
    }

    public void setFriendUserId(Long friendUserId) {
        this.friendUserId = friendUserId;
    }

    @Override
    public String toString() {
        return "ParticipantFriend: " +
                " screen name " + getScreenName() +
                " User id " + getUserId() +
                " Friend user Id " + getFriendUserId() +
                " Friend screen name " + getFriendScreenName() +
                " [User data]" + super.toString();
    }
}
