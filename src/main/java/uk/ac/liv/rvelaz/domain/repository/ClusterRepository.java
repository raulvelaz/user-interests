package uk.ac.liv.rvelaz.domain.repository;

import moa.cluster.Cluster;
import moa.clusterers.clustree.ClusKernel;
import org.apache.log4j.Logger;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import uk.ac.liv.rvelaz.domain.Participant;
import uk.ac.liv.rvelaz.infrastructure.repository.GenericRepository;

import java.util.List;

/**
 *
 * User: raul
 * Date: 21/11/2012
 * Time: 18:33
 */
public class ClusterRepository extends GenericRepository<ClusKernel> {
    private final static Logger logger = Logger.getLogger(ClusterRepository.class);

     private String collection;

    public ClusterRepository(Class<ClusKernel> clazz, String collection) {
        super(clazz);
        this.collection = collection;
    }

    public ClusterRepository(String collection) {
        super(ClusKernel.class);
        this.collection = collection;
    }

    @Override
    public String getCollection() {
        return this.collection;
    }

}
