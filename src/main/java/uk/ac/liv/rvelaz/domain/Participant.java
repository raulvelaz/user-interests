package uk.ac.liv.rvelaz.domain;

import java.util.List;
import java.util.Set;

/**
 * User: raul
 * Date: 21/11/2012
 * Time: 11:56
 */
public class Participant {
    private Long userId;
    private String email;
    private String screenName;
    private boolean controlGroup;
    private Set<String> topics;
    private List<Long> friendsIds;
    private int followersCount;
    private int statusesCount;

    public boolean isControlGroup() {
        return controlGroup;
    }

    public void setControlGroup(boolean controlGroup) {
        this.controlGroup = controlGroup;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<Long> getFriendsIds() {
        return friendsIds;
    }

    public void setFriendsIds(List<Long> friendsIds) {
        this.friendsIds = friendsIds;
    }

    public String getScreenName() {
        return screenName;
    }

    public void setScreenName(String screenName) {
        this.screenName = screenName;
    }

    public Set<String> getTopics() {
        return topics;
    }

    public void setTopics(Set<String> topics) {
        this.topics = topics;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public int getFollowersCount() {
        return followersCount;
    }

    public void setFollowersCount(int followersCount) {
        this.followersCount = followersCount;
    }

    public int getStatusesCount() {
        return statusesCount;
    }

    public void setStatusesCount(int statusesCount) {
        this.statusesCount = statusesCount;
    }

    @Override
    public String toString() {
        return "Participant: email " + getEmail() +
                " screen name " + getScreenName() +
                " control group " + isControlGroup();
    }

    @Override
    public boolean equals(Object o) {
        if(o instanceof Participant){
            Participant participant = (Participant)o;
            return participant.getUserId().equals(this.getUserId());
        }
        else{
            return false;
        }

    }
}
