package uk.ac.liv.rvelaz.domain.repository;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.sun.org.apache.xpath.internal.operations.Bool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import twitter4j.GeoLocation;
import twitter4j.Status;
import uk.ac.liv.rvelaz.common.transformer.StatusTransformer;
import uk.ac.liv.rvelaz.domain.Tweet;
import uk.ac.liv.rvelaz.domain.User;

import java.util.Date;

/**
 * User: raul
 * Date: 14/12/2012
 * Time: 09:58
 *
 * Converter used by the spring framework to transform Status objects
 * saved into the DB to the domain class Tweet
 *
 */
public class StatusReadConverter implements Converter<DBObject, Tweet> {
    @Autowired
    private StatusTransformer statusTransformer;

    /**
     * Transform an object retrieved from the DB into a Tweet user. This allows to
     * store the whole information about a tweet coming from Twitter and just
     * use in the application the parts that are needed.
     * @param source
     * @return
     */
    @Override
    public Tweet convert(DBObject source) {
        Tweet tweet = new Tweet();
        tweet.setId((Long)source.get("_id"));
        tweet.setText((String) source.get("text"));
        tweet.setCreatedAt((Date) source.get("createdAt"));
        tweet.setFavorited((Boolean) source.get("isFavorited"));
        tweet.setRetweetCount((Long) source.get("retweetCount"));
        tweet.setSource((String)source.get("source"));


        DBObject userDb = (DBObject)source.get("user");
        User user = new User();
        user.setId((Long)userDb.get("_id"));
        user.setScreenName((String)userDb.get("screenName"));

        tweet.setUser(user);
        return tweet;
    }
}
