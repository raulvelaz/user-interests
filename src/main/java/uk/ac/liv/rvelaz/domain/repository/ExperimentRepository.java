package uk.ac.liv.rvelaz.domain.repository;

import moa.clusterers.clustree.ClusKernel;
import org.apache.log4j.Logger;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import uk.ac.liv.rvelaz.domain.Experiment;
import uk.ac.liv.rvelaz.domain.Participant;
import uk.ac.liv.rvelaz.infrastructure.repository.GenericRepository;

/**
 *
 * User: raul
 * Date: 21/11/2012
 * Time: 18:33
 */
public class ExperimentRepository extends GenericRepository<Experiment> {
    private final static Logger logger = Logger.getLogger(ExperimentRepository.class);

     private String collection;

    public ExperimentRepository(Class<Experiment> clazz, String collection) {
        super(clazz);
        this.collection = collection;
    }

    public ExperimentRepository(String collection) {
        super(Experiment.class);
        this.collection = collection;
    }

    @Override
    public String getCollection() {
        return this.collection;
    }

     public boolean hasExperiment(Participant participant) {
        Criteria criteria = Criteria.where("participant.userId").is(participant.getUserId());
        Query query = new Query(criteria);
        Participant participantRes = mongoTemplate.findOne(query, Participant.class, collection);
         if(participantRes == null)
             return false;
         else
             return true;

    }


}
