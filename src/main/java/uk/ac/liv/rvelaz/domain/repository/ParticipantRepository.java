package uk.ac.liv.rvelaz.domain.repository;

import org.apache.log4j.Logger;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Order;
import org.springframework.data.mongodb.core.query.Query;
import uk.ac.liv.rvelaz.domain.Participant;
import uk.ac.liv.rvelaz.infrastructure.repository.GenericRepository;

import java.util.List;

/**
 *
 * User: raul
 * Date: 21/11/2012
 * Time: 18:33
 *
 * Repository class that manages research participants
 *
 */
public class ParticipantRepository extends GenericRepository<Participant> {
    private final static Logger logger = Logger.getLogger(ParticipantRepository.class);

     private String collection;

    public ParticipantRepository(Class<Participant> clazz, String collection) {
        super(clazz);
        this.collection = collection;
    }

    public ParticipantRepository(String collection) {
        super(Participant.class);
        this.collection = collection;
    }

    @Override
    public String getCollection() {
        return this.collection;
    }

    /**
     * Get a the list of research participants
     * @return list of research participants
     */
    public List<Participant> getResearchParticipants() {
        Criteria criteria = Criteria.where("_class").is("uk.ac.liv.rvelaz.domain.Participant");
        Query query = new Query(criteria);
        return mongoTemplate.find(query, Participant.class, collection);
    }

    /**
     * Get the Twitter friends of a research participant
     * @param participant
     * @return
     */
    public List<Participant> getFriendsForParticipant(Participant participant) {
        Criteria criteria = Criteria.where("_class").is("uk.ac.liv.rvelaz.domain.ParticipantFriend").
                                and("friendScreenName").is(participant.getScreenName());

        Query query = new Query(criteria);
        return mongoTemplate.find(query, Participant.class, collection);
    }

    /**
     * Get a research participant using the Twitter user ID
     * @param userId Twitter user ID
     * @return research participant
     */
    public Participant getParticipantById(Long userId) {
        Criteria criteria = Criteria.where("userId").is(userId);
        Query query = new Query(criteria);
        return mongoTemplate.findOne(query, Participant.class, collection);
    }

    public List<Participant> getTestParticipants() {
        Criteria criteria = Criteria.where("topics").is("no topic").and("_class").is("uk.ac.liv.rvelaz.domain.Participant");
        Query query = new Query(criteria);
        return mongoTemplate.find(query, Participant.class, collection);
    }
}
