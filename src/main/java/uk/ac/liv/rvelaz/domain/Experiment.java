package uk.ac.liv.rvelaz.domain;

import java.util.List;

/**
 * User: raul
 * Date: 07/12/2012
 * Time: 08:51
 *
 * Information regarding the experiment run for a research participant
 *
 */
public class Experiment {
    private enum Algorithm {
        ALG1,
        ALG2
    }
    private Participant participant;
    private int numberOfTweets;
    private List<String> topics;
    private Algorithm algorithm;


    public Algorithm getAlgorithm() {
        return algorithm;
    }

    public void setAlgorithm(Algorithm algorithm) {
        this.algorithm = algorithm;
    }

    public int getNumberOfTweets() {
        return numberOfTweets;
    }

    public void setNumberOfTweets(int numberOfTweets) {
        this.numberOfTweets = numberOfTweets;
    }

    public Participant getParticipant() {
        return participant;
    }

    public void setParticipant(Participant participant) {
        this.participant = participant;
    }

    public List<String> getTopics() {
        return topics;
    }

    public void setTopics(List<String> topics) {
        this.topics = topics;
    }
}
