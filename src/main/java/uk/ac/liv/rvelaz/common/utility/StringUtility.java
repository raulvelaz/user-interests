package uk.ac.liv.rvelaz.common.utility;

import java.util.ArrayList;
import java.util.List;

/**
 * User: raul Date: 21/10/2011 Time: 08:33
 */
public class StringUtility {

	/**
	 * Filters a words that contain numbers one-letter words, words that have symbols
	 * inside words and remove ' at the end of word
	 * 
	 * @param word
	 * @return filtered words in lowercase
	 */
    //use filterWordExtended
	public static String filterWord(String word) {
		// remove last character when it is a '
		if (word.matches(".*'")) {
            if(word.equals("don'") || word.equals("doesn'") || word.equals("didn'") ){
                return "dont";
            }
            if(word.equals("can'") || word.equals("couldn'")){
                return "cant";
            }
			    word = word.subSequence(0, word.length() - 1).toString();
		}
		// remove one letter words
		if (word.length() == 1)
			return "";
		if (word.length() == 2
				&& (!word.equals("ok") || !word.equals("ko") || word
						.equals("xx")))
			return "";
		if (word.matches(".*[:|_|-]+.*"))
			return "";
		if (word.matches(".*(\\..*)+"))
			return "";
		if (word.matches(".*[0-9+].*"))
			return "";
		return word.toLowerCase();
	}

    /**
     * Like filterWord but also removes mentions to other users, e.g. @what_ever
     * @param word
     * @return filtered word
     */
    public static String filterWordExtended(String word) {
        if (word.matches("\\@.*"))
			return "";
        return filterWord(word);
    }

	/**
	 * Filters a list of words that contain numbers one-letter words, words that
	 * symbols inside words kkks:ss letters with . inside remove ' at the end of
	 * word
	 * 
	 * @param words
	 * @return filtered words in lowercase
	 */
    @Deprecated
	public static String[] filterWords(String[] words) {
		List<String> list = new ArrayList<String>();
		for (int i = 0; i < words.length; i++) {
			String filtered = filterWord(words[i]);
			if (!filtered.isEmpty())
				list.add(filtered);
		}
		return list.toArray(new String[list.size()]);
	}

	public static String filterWordsToString(String text) {
		String[] words = text.split(" ");
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < words.length; i++) {
			String filtered = filterWordExtended(words[i]);
			if (!filtered.isEmpty()) {
				sb.append(filtered);
				sb.append(" ");
			}
		}
		return sb.toString();
	}
}
