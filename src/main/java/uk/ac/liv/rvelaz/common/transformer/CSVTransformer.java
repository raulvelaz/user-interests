package uk.ac.liv.rvelaz.common.transformer;

import au.com.bytecode.opencsv.CSVReader;
import org.apache.log4j.Logger;
import uk.ac.liv.rvelaz.common.to.ParticipantTO;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * User: raul
 * Date: 21/11/2012
 * Time: 12:26
 *
 * Reads a CSV file containing a list of research participants and transforms it into
 * a list of ParticipantTO
 *
 *
 * The CSV file has to have this format
 * email;Twitter screen name; control; list of topics
 *
 */
public class CSVTransformer {
    private final static Logger logger = Logger.getLogger(CSVTransformer.class);

    private static final Integer EMAIL = 0;
    private static final Integer SCREEN_NAME = 1;
    private static final Integer CONTROL = 2;
    private static final Integer TOPICS = 3;

    private String file;

    public CSVTransformer(String file){
        this.file = file;
    }


    /**
     * Get the list of research participants
     * @param file CSV file containing the list of research participants
     * @return list of research participants
     */
    public List<ParticipantTO> getParticipants(String file) {
        List<ParticipantTO> participantTOs = getParticipantsFromCSV(file);

        return participantTOs;


    }

    /**
     * Get the list of research participants from the default CSV list of participants
     * in the resources
     * @return list of research participants
     */
    public List<ParticipantTO> getParticipants() {
        URL fileLocation = this.getClass().getResource("/connector/" + file);
        String filePath = fileLocation.getFile();

        List<ParticipantTO> participantTOs = getParticipantsFromCSV(filePath);

        return participantTOs;
    }


    private  List<ParticipantTO> getParticipantsFromCSV(String file){
        CSVReader reader = null;
        List<ParticipantTO> participantTOs = new ArrayList<ParticipantTO>();

        try {
            //File delimiter is ; and skip the first line, which contains the header
            reader = new CSVReader(new FileReader(file), ';', '\'', 1);
            String[] nextLine;
                while ((nextLine = reader.readNext()) != null) {
                    String email = nextLine[EMAIL];
                    String screenName = nextLine[SCREEN_NAME];
                    boolean control = Boolean.valueOf(nextLine[CONTROL]);
                    String topics = nextLine[TOPICS];

                    //The list of topics is separated using ,
                    Set<String> topicSet = new HashSet<String>();
                    if (topics.contains(",")) {
                        for(String topic : topics.split(",")){
                            topicSet.add(topic);
                        }
                    }
                    else {
                        topicSet.add(topics);
                    }
                    //Build participant TO object
                    ParticipantTO participantTO = new ParticipantTO();
                    participantTO.setControlGroup(control);
                    participantTO.setEmail(email);
                    participantTO.setScreenName(screenName);
                    participantTO.setTopics(topicSet);

                    participantTOs.add(participantTO);

                }


        } catch (FileNotFoundException e) {
            logger.error("File not found: " + e.getMessage());
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
        return participantTOs;
    }
}
