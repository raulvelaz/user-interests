package uk.ac.liv.rvelaz.common.to;

import java.io.Serializable;
import java.util.Set;

/**
 * User: raul
 * Date: 21/11/2012
 * Time: 11:56
 *
 * Transfer Object that represents a research participant
 *
 */
public class ParticipantTO implements Serializable {
    private String email;
    private String screenName;
    private boolean controlGroup;
    private Set<String> topics;

    public boolean isControlGroup() {
        return controlGroup;
    }

    public void setControlGroup(boolean controlGroup) {
        this.controlGroup = controlGroup;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


    public String getScreenName() {
        return screenName;
    }

    public void setScreenName(String screenName) {
        this.screenName = screenName;
    }

    public Set<String> getTopics() {
        return topics;
    }

    public void setTopics(Set<String> topics) {
        this.topics = topics;
    }
}
