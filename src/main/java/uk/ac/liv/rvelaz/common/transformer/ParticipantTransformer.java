package uk.ac.liv.rvelaz.common.transformer;

import uk.ac.liv.rvelaz.domain.Participant;
import uk.ac.liv.rvelaz.common.to.ParticipantTO;

/**
 * User: raul
 * Date: 21/11/2012
 * Time: 17:03
 *
 * Transforms a ParticipantTO into the domain class Participant
 *
 */
public class ParticipantTransformer {

    /**
     * Get a research participant from a research participant TO
     * @param participantTO participant transfer object
     * @return research participant
     */
    public Participant transformTO(ParticipantTO participantTO){
        Participant participant = new Participant();
        participant.setControlGroup(participantTO.isControlGroup());
        participant.setEmail(participantTO.getEmail());
        participant.setScreenName(participantTO.getScreenName());
        participant.setTopics(participantTO.getTopics());
        return participant;
    }
}
