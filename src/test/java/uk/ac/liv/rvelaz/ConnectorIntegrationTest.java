package uk.ac.liv.rvelaz;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.Message;
import org.springframework.integration.MessageChannel;
import org.springframework.integration.channel.ChannelInterceptor;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.channel.PublishSubscribeChannel;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import twitter4j.Status;
import twitter4j.TwitterException;
import uk.ac.liv.rvelaz.common.transformer.StatusTransformer;
import uk.ac.liv.rvelaz.domain.Participant;
import uk.ac.liv.rvelaz.domain.ParticipantFriend;
import uk.ac.liv.rvelaz.domain.Tweet;
import uk.ac.liv.rvelaz.domain.repository.ParticipantRepository;
import uk.ac.liv.rvelaz.domain.repository.TweetRepository;
import uk.ac.liv.rvelaz.service.*;
import uk.ac.liv.rvelaz.common.to.ParticipantTO;
import uk.ac.liv.rvelaz.common.transformer.ParticipantTransformer;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

/**
 * User: raul
 * Date: 21/11/2012
 * Time: 15:38
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:domain/repository/repository-test.xml",
        "classpath:connector/connector-application-context-test.xml"})
public class ConnectorIntegrationTest {


    @Autowired
    private DirectChannel saveFriendsChannel;

    @Autowired
    private PublishSubscribeChannel participantsChannel;

    @Autowired
    private RequestFriends requestFriends;

    @Autowired
    private ParticipantTransformer participantTransformer;

    @Autowired
    private ParticipantRepository participantRepository;

    @Autowired
    private TweetRepository tweetRepository;

    @Autowired
    private FriendRequester friendRequester;

    @Autowired
    private TweetRequester tweetRequester;

    @Autowired
    private StatusTransformer statusTransformer;

    @Before
    public void setUp() {
        reset(participantRepository);
        reset(tweetRepository);
        reset(participantTransformer);
        reset(tweetRequester);
        reset(friendRequester);

    }

    @Test
    public void friendRequestsAreSentToRateControllerWhenCsvListIsRead() throws InterruptedException, TwitterException {
        List<Participant> participantList = new ArrayList<Participant>();
        Participant participant = mock(Participant.class);
        participantList.add(participant);

        when(participant.getFriendsIds()).thenReturn(new ArrayList<Long>());
        when(participantTransformer.transformTO(any(ParticipantTO.class))).thenReturn(participant);

        when(friendRequester.requestFriends(participant)).thenReturn(participantList);

        requestFriends.requestFriendsForParticipants();

        //There are four entries in the test csv
        verify(participantRepository, times(4)).add(any(Participant.class));
    }

    @Ignore
    @Test
    public void participantsWhoseDetailsWereNotObtainedAreFilteredOut() {
        Participant participant = new Participant();
        Message<Participant> message = MessageBuilder.withPayload(participant).build();
        final MockService mockService = mock(MockService.class);

        ChannelInterceptor channelInterceptor = new ChannelInterceptor() {
            @Override
            public Message<?> preSend(Message<?> message, MessageChannel channel) {
                mockService.doService();
                return message;
            }

            @Override
            public void postSend(Message<?> message, MessageChannel channel, boolean sent) {
            }

            @Override
            public boolean preReceive(MessageChannel channel) {
                return false;
            }

            @Override
            public Message<?> postReceive(Message<?> message, MessageChannel channel) {
                return message;
            }
        };

        participantsChannel.addInterceptor(channelInterceptor);

        saveFriendsChannel.send(message);

        verify(mockService, never()).doService();

    }

    @Test
    public void whenFriendsAreObtainedTweetsAreRequested() throws InterruptedException, TwitterException {
        List<Participant> participantList = new ArrayList<Participant>();
        Participant participant = mock(Participant.class);
        ParticipantFriend participantFriend = mock(ParticipantFriend.class);

        participantList.add(participant);
        participantList.add(participantFriend);

        List<Status> statuses = new ArrayList<Status>();
        Status status1 = mock(Status.class);
        Status status2 = mock(Status.class);
        Status status3 = mock(Status.class);
        Status status4 = mock(Status.class);
        Status status5 = mock(Status.class);

        statuses.add(status1);
        statuses.add(status2);
        statuses.add(status3);
        statuses.add(status4);
        statuses.add(status5);

        Tweet tweet = mock(Tweet.class);

        when(participant.getFriendsIds()).thenReturn(new ArrayList<Long>());
        when(participantTransformer.transformTO(any(ParticipantTO.class))).thenReturn(participant);

        when(friendRequester.requestFriends(participant)).thenReturn(participantList);

        when(tweetRequester.requestTweets(any(Participant.class))).thenReturn(statuses);

        when(statusTransformer.transform(any(Status.class))).thenReturn(tweet);


        requestFriends.requestFriendsForParticipants();
        Thread.sleep(100);

        //There are four entries in the test csv
        verify(tweetRequester, times(8)).requestTweets(any(Participant.class));


        verify(statusTransformer, times(40)).transform(any(Status.class));

        verify(tweetRepository, times(40)).add(any(Tweet.class));


    }

}
