package uk.ac.liv.rvelaz;

import com.cybozu.labs.langdetect.LangDetectException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.Message;
import org.springframework.integration.MessageChannel;
import org.springframework.integration.channel.ChannelInterceptor;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.support.MessageBuilder;
import static org.springframework.integration.test.matcher.MockitoMessageMatchers.*;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import uk.ac.liv.rvelaz.clustering.ICluster;
import uk.ac.liv.rvelaz.domain.Participant;
import uk.ac.liv.rvelaz.domain.Tweet;
import uk.ac.liv.rvelaz.domain.User;
import uk.ac.liv.rvelaz.domain.repository.ExperimentRepository;
import uk.ac.liv.rvelaz.domain.repository.ParticipantRepository;
import uk.ac.liv.rvelaz.infrastructure.integration.LanguageFilter;
import uk.ac.liv.rvelaz.service.ExperimentRunner;
import uk.ac.liv.rvelaz.service.MockService;
import uk.ac.liv.rvelaz.service.TweetCorpusFinder;
import uk.ac.liv.rvelaz.service.TwitterStreamSimulator;

import java.io.IOException;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.mockito.Mockito.*;

/**
 * User: raul
 * Date: 07/12/2012
 * Time: 09:19
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:streaming/streaming-flow.xml", "classpath:streaming/streaming-services-test.xml",
        "classpath:domain/repository/embedded-mongo-context.xml"})
public class StreamingSimulatorIntegrationTest {

    @Autowired
    private TwitterStreamSimulator twitterStreamSimulator;

    @Autowired
    private ParticipantRepository participantRepository;

    @Autowired
    private ExperimentRepository experimentRepository;

    @Autowired
    private ExperimentRunner experimentRunner;

    @Autowired
    private LanguageFilter languageFilter;

    @Autowired
    private DirectChannel participantTweetCorpusChannel;

    @Autowired
    private DirectChannel clusteringChannel;

    @Autowired
    private TweetCorpusFinder tweetCorpusFinder;

    @Test
    public void participantsArriveToExperimentChannel() {

        Participant participant1 = mock(Participant.class);
        Participant participant2 = mock(Participant.class);
        when(participant1.getUserId()).thenReturn(1L);
        when(participant2.getUserId()).thenReturn(2L);


        List<Participant> participants = new ArrayList<Participant>();
        participants.add(participant1);
        participants.add(participant2);

        when(participantRepository.getTestParticipants()).thenReturn(participants);
        when(experimentRepository.hasExperiment(any(Participant.class))).thenReturn(false);

        MockService mockService = mock(MockService.class);

        participantTweetCorpusChannel.addInterceptor(createInterceptor(mockService));

        twitterStreamSimulator.simulateStream();

        verify(mockService, times(2)).doService();
    }

    @Test
    public void tweetCorpusForParticipantIsObtainedAndIsSentToClusteringAlgorithmsInOrder() throws LangDetectException, IOException {

        Participant participant1 = mock(Participant.class);
        Participant participant2 = mock(Participant.class);
        Participant participant3 = mock(Participant.class);

        when(participant1.getUserId()).thenReturn(1L);
        when(participant2.getUserId()).thenReturn(2L);
        when(participant3.getUserId()).thenReturn(3L);

        List<Participant> participants = new ArrayList<Participant>();
        participants.add(participant1);
        participants.add(participant2);
        participants.add(participant3);

        when(participantRepository.getTestParticipants()).thenReturn(participants);

        List<Tweet> tweets1 = createTweets(1L, "participant1", 6);
        List<Tweet> tweets2 = createTweets(3L, "participant2", 10);
        List<Tweet> tweets3 = createTweets(2L, "participant3", 3);


        Message<List<Tweet>> message1 = MessageBuilder.withPayload(tweets1)
                                                       .setHeader("participant", 1L)
                                                       .build();
        Message<List<Tweet>> message2 = MessageBuilder.withPayload(tweets2)
                                                       .setHeader("participant", 2L)
                                                       .build();
        Message<List<Tweet>> message3 = MessageBuilder.withPayload(tweets3)
                                                       .setHeader("participant", 3L)
                                                       .build();


        when(tweetCorpusFinder.getTweets(participant1)).thenReturn(message1);
        when(tweetCorpusFinder.getTweets(participant2)).thenReturn(message2);
        when(tweetCorpusFinder.getTweets(participant3)).thenReturn(message3);

        when(languageFilter.filter(tweets1.get(0))).thenReturn(true);
        when(languageFilter.filter(tweets1.get(1))).thenReturn(true);
        when(languageFilter.filter(tweets1.get(2))).thenReturn(true);
        when(languageFilter.filter(tweets1.get(3))).thenReturn(true);
        when(languageFilter.filter(tweets1.get(4))).thenReturn(true);
        when(languageFilter.filter(tweets1.get(5))).thenReturn(true);
        when(languageFilter.filter(tweets2.get(0))).thenReturn(true);
        when(languageFilter.filter(tweets2.get(0))).thenReturn(true);
        when(languageFilter.filter(tweets2.get(1))).thenReturn(true);
        when(languageFilter.filter(tweets2.get(2))).thenReturn(true);
        when(languageFilter.filter(tweets2.get(3))).thenReturn(true);
        when(languageFilter.filter(tweets2.get(4))).thenReturn(true);
        when(languageFilter.filter(tweets2.get(5))).thenReturn(true);
        when(languageFilter.filter(tweets2.get(6))).thenReturn(true);
        when(languageFilter.filter(tweets2.get(7))).thenReturn(true);
        when(languageFilter.filter(tweets2.get(8))).thenReturn(true);
        when(languageFilter.filter(tweets2.get(9))).thenReturn(true);
        when(languageFilter.filter(tweets3.get(0))).thenReturn(true);
        when(languageFilter.filter(tweets3.get(1))).thenReturn(true);
        when(languageFilter.filter(tweets3.get(2))).thenReturn(true);

        MockService mockService = mock(MockService.class);

        clusteringChannel.addInterceptor(createInterceptor(mockService));

        twitterStreamSimulator.simulateStream();

        verify(tweetCorpusFinder, times(1)).getTweets(participant1);
        verify(tweetCorpusFinder, times(1)).getTweets(participant2);
        verify(tweetCorpusFinder, times(1)).getTweets(participant3);

        InOrder  inOrder = inOrder(tweetCorpusFinder);
        inOrder.verify(tweetCorpusFinder).getTweets(participant1);
        inOrder.verify(tweetCorpusFinder).getTweets(participant2);
        inOrder.verify(tweetCorpusFinder).getTweets(participant3);

        verify(mockService, times(19)).doService();

        verify(languageFilter, times(19)).filter(any(Tweet.class));

        verify(experimentRunner, times(19)).startOrContinueExperiment(any(Message.class));

        InOrder inOrderCluster = inOrder(experimentRunner);


        inOrderCluster.verify(experimentRunner).startOrContinueExperiment(messageWithPayload(tweets1.get(0)));
        inOrderCluster.verify(experimentRunner).startOrContinueExperiment(messageWithPayload(tweets1.get(1)));
        inOrderCluster.verify(experimentRunner).startOrContinueExperiment(messageWithPayload(tweets1.get(2)));
        inOrderCluster.verify(experimentRunner).startOrContinueExperiment(messageWithPayload(tweets1.get(3)));
        inOrderCluster.verify(experimentRunner).startOrContinueExperiment(messageWithPayload(tweets1.get(4)));
        inOrderCluster.verify(experimentRunner).startOrContinueExperiment(messageWithPayload(tweets1.get(5)));
        inOrderCluster.verify(experimentRunner).startOrContinueExperiment(messageWithPayload(tweets2.get(0)));
        inOrderCluster.verify(experimentRunner).startOrContinueExperiment(messageWithPayload(tweets2.get(1)));
        inOrderCluster.verify(experimentRunner).startOrContinueExperiment(messageWithPayload(tweets2.get(2)));
        inOrderCluster.verify(experimentRunner).startOrContinueExperiment(messageWithPayload(tweets2.get(3)));
        inOrderCluster.verify(experimentRunner).startOrContinueExperiment(messageWithPayload(tweets2.get(4)));
        inOrderCluster.verify(experimentRunner).startOrContinueExperiment(messageWithPayload(tweets2.get(5)));
        inOrderCluster.verify(experimentRunner).startOrContinueExperiment(messageWithPayload(tweets2.get(6)));
        inOrderCluster.verify(experimentRunner).startOrContinueExperiment(messageWithPayload(tweets2.get(7)));
        inOrderCluster.verify(experimentRunner).startOrContinueExperiment(messageWithPayload(tweets2.get(8)));
        inOrderCluster.verify(experimentRunner).startOrContinueExperiment(messageWithPayload(tweets2.get(9)));
        inOrderCluster.verify(experimentRunner).startOrContinueExperiment(messageWithPayload(tweets3.get(0)));
        inOrderCluster.verify(experimentRunner).startOrContinueExperiment(messageWithPayload(tweets3.get(1)));
        inOrderCluster.verify(experimentRunner).startOrContinueExperiment(messageWithPayload(tweets3.get(2)));

    }

    private List<Tweet> createTweets(Long userId, String screenName, int numberOfTweets) {
        List<Tweet> tweets = new ArrayList<Tweet>();

        for (int i = 0; i < numberOfTweets; i++) {
            SecureRandom random = new SecureRandom();
            String text = new BigInteger(130, random).toString(32);

            Tweet tweet = createTweet(text, userId, screenName);
            tweets.add(tweet);
        }

        return tweets;

    }

    private Tweet createTweet(String text, Long userId, String screenName) {
        User user = new User();
        user.setCreatedAt(new Date());
        user.setId(userId);
        user.setScreenName(screenName);

        Tweet tweet = new Tweet();
        tweet.setText(text);
        tweet.setUser(user);
        tweet.setId(Double.doubleToLongBits(Math.random() * 100));

        return tweet;
    }


    private ChannelInterceptor createInterceptor(final MockService mockService) {

        ChannelInterceptor channelInterceptor = new ChannelInterceptor() {

            @Override
            public Message<?> preSend(Message<?> message, MessageChannel channel) {
                mockService.doService();
                return message;
            }

            @Override
            public void postSend(Message<?> message, MessageChannel channel, boolean sent) {

            }

            @Override
            public boolean preReceive(MessageChannel channel) {
                return false;
            }

            @Override
            public Message<?> postReceive(Message<?> message, MessageChannel channel) {
                return null;
            }
        };
        return channelInterceptor;
    }
}
