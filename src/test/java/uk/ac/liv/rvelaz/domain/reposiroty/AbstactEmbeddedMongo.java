package uk.ac.liv.rvelaz.domain.reposiroty;

import de.flapdoodle.embedmongo.MongoDBRuntime;
import de.flapdoodle.embedmongo.MongodExecutable;
import de.flapdoodle.embedmongo.MongodProcess;
import de.flapdoodle.embedmongo.config.MongodConfig;
import de.flapdoodle.embedmongo.distribution.Version;
import de.flapdoodle.embedmongo.runtime.Network;
import org.junit.After;
import org.junit.Before;

/**
 * User: raul
 * Date: 20/11/2012
 * Time: 17:27
 */
public abstract class AbstactEmbeddedMongo {


    private static final String DATABASE_NAME = "embedded";

    private MongodExecutable mongodExe;
    private MongodProcess mongod;

    @Before
    public void beforeEach() throws Exception {
        MongoDBRuntime runtime = MongoDBRuntime.getDefaultInstance();
        mongodExe = runtime.prepare(new MongodConfig(Version.V2_0, 12345, Network.localhostIsIPv6()));
        mongod = mongodExe.start();

    }

    @After
    public void afterEach() throws Exception {
        mongod.stop();
        mongodExe.cleanup();
    }

}