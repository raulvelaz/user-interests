package uk.ac.liv.rvelaz.domain.reposiroty;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import uk.ac.liv.rvelaz.domain.Participant;
import uk.ac.liv.rvelaz.domain.ParticipantFriend;
import uk.ac.liv.rvelaz.domain.repository.ParticipantRepository;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;

/**
 * User: raul
 * Date: 28/11/2012
 * Time: 11:55
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:domain/repository/embedded-mongo-context.xml",
        "classpath:domain/repository/repository-config.xml"})
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class ParticipantRepositoryUnitTest extends AbstactEmbeddedMongo {

    @Autowired
    private ParticipantRepository participantRepository;

    @Test
    public void saveParticipant() {
        String screenName = "joe";
        boolean control = false;
        String email = "email@email.com";
        List<Long> friendsIds = new ArrayList<Long>();
        friendsIds.add(1L);
        friendsIds.add(2L);

        Set<String> topics = new HashSet<String>();
        topics.add("topic1");
        topics.add("topic2");

        Participant participant = new Participant();
        participant.setScreenName(screenName);
        participant.setControlGroup(control);
        participant.setEmail(email);
        participant.setFriendsIds(friendsIds);
        participant.setTopics(topics);

        Participant participant1 = new Participant();
        participant1.setScreenName("screen");
        participant1.setControlGroup(control);
        participant1.setEmail(email);
        participant1.setFriendsIds(friendsIds);


        participantRepository.add(participant);
        participantRepository.add(participant1);

        List<Participant> participants = participantRepository.getAll();
        assertEquals(2, participants.size());
        Participant participantRetrieved = participantRepository.getAll().get(0);

        assertEquals(screenName, participantRetrieved.getScreenName());
        assertEquals(email, participantRetrieved.getEmail());
        assertEquals(2, participantRetrieved.getFriendsIds().size());
        assertEquals(2, participantRetrieved.getTopics().size());


    }

    @Test
    public void getAllParticipants() {
        String screenName = "joe";
        boolean control = false;
        String email = "email@email.com";
        List<Long> friendsIds = new ArrayList<Long>();
        friendsIds.add(1L);
        friendsIds.add(2L);

        Set<String> topics = new HashSet<String>();
        topics.add("topic1");
        topics.add("topic2");

        Participant participant = new Participant();
        participant.setScreenName(screenName);
        participant.setControlGroup(control);
        participant.setEmail(email);
        participant.setFriendsIds(friendsIds);
        participant.setTopics(topics);

        ParticipantFriend participantFriend = new ParticipantFriend();
        participantFriend.setScreenName("participant1");
        participantFriend.setFriendScreenName("joe");
        participantFriend.setFriendUserId(3L);
        participantFriend.setUserId(1L);

        ParticipantFriend participantFriend2 = new ParticipantFriend();
        participantFriend2.setScreenName("participant1");
        participantFriend2.setFriendScreenName("joe");
        participantFriend2.setFriendUserId(3L);
        participantFriend2.setUserId(1L);

        participantRepository.add(participant);
        participantRepository.add(participantFriend);
        participantRepository.add(participantFriend2);

        assertEquals(1, participantRepository.getResearchParticipants().size());

    }

    @Test
    public void getParticipantFriends() {
        String screenName = "joe";
        boolean control = false;
        String email = "email@email.com";
        List<Long> friendsIds = new ArrayList<Long>();
        friendsIds.add(1L);
        friendsIds.add(2L);

        Set<String> topics = new HashSet<String>();
        topics.add("topic1");
        topics.add("topic2");

        Participant participant = new Participant();
        participant.setScreenName(screenName);
        participant.setControlGroup(control);
        participant.setEmail(email);
        participant.setFriendsIds(friendsIds);
        participant.setTopics(topics);
        participant.setUserId(10L);

        ParticipantFriend participantFriend = new ParticipantFriend();
        participantFriend.setScreenName("participant1");
        participantFriend.setFriendScreenName("joe");
        participantFriend.setFriendUserId(10L);
        participantFriend.setUserId(1L);

        ParticipantFriend participantFriend2 = new ParticipantFriend();
        participantFriend2.setScreenName("participant2");
        participantFriend2.setFriendScreenName("unknown");
        participantFriend2.setFriendUserId(3L);
        participantFriend2.setUserId(3L);

        participantRepository.add(participant);
        participantRepository.add(participantFriend);
        participantRepository.add(participantFriend2);


        List<Participant> participants = participantRepository.getFriendsForParticipant(participant);
        assertEquals(1, participants.size());
        assertEquals("participant1", participants.get(0).getScreenName());

    }

    @Test
    public void getParticipantById() {

        String screenName = "joe";
        boolean control = false;
        Long userId = 10L;
        String email = "email@email.com";
        List<Long> friendsIds = new ArrayList<Long>();
        friendsIds.add(1L);
        friendsIds.add(2L);

        Set<String> topics = new HashSet<String>();
        topics.add("topic1");
        topics.add("topic2");

        Participant participant = new Participant();
        participant.setScreenName(screenName);
        participant.setControlGroup(control);
        participant.setEmail(email);
        participant.setFriendsIds(friendsIds);
        participant.setTopics(topics);
        participant.setUserId(userId);

        ParticipantFriend participantFriend = new ParticipantFriend();
        participantFriend.setScreenName("participant1");
        participantFriend.setFriendScreenName("joe");
        participantFriend.setFriendUserId(10L);
        participantFriend.setUserId(1L);

        ParticipantFriend participantFriend2 = new ParticipantFriend();
        participantFriend2.setScreenName("participant2");
        participantFriend2.setFriendScreenName("unknown");
        participantFriend2.setFriendUserId(3L);
        participantFriend2.setUserId(3L);

        participantRepository.add(participant);
        participantRepository.add(participantFriend);
        participantRepository.add(participantFriend2);

        Participant participantDB = participantRepository.getParticipantById(participant.getUserId());

        assertEquals(screenName, participantDB.getScreenName());
        assertEquals(userId, participantDB.getUserId());
        assertEquals(email, participantDB.getEmail());

    }

    @Test
    public void testParticipants() {
        String screenName = "joe";
        boolean control = false;
        Long userId = 10L;
        String email = "email@email.com";
        Set<String> topics = new HashSet<String>();
        topics.add("topic1");
        topics.add("topic2");
        List<Long> friendsIds = new ArrayList<Long>();
        friendsIds.add(1L);
        friendsIds.add(2L);

        Participant participant = new Participant();
        participant.setScreenName(screenName);
        participant.setControlGroup(true);
        participant.setEmail(email);
        participant.setFriendsIds(friendsIds);
        participant.setTopics(topics);
        participant.setUserId(userId);

        Participant participant1 = new Participant();
        participant1.setScreenName(screenName);
        participant1.setControlGroup(false);
        participant1.setEmail(email);
        participant1.setFriendsIds(friendsIds);
        Set<String> topics1 = new HashSet<String>();
        topics1.add("no topic");
        participant1.setTopics(topics1);
        participant1.setUserId(1L);

        participantRepository.add(participant);
        participantRepository.add(participant1);

        List<Participant> testUsers = participantRepository.getTestParticipants();

        assertEquals(1, testUsers.size());
        assertEquals(1L, (long)testUsers.get(0).getUserId());


    }

}
