package uk.ac.liv.rvelaz.domain.reposiroty;

import junit.framework.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import twitter4j.UserMentionEntity;
import uk.ac.liv.rvelaz.domain.*;
import uk.ac.liv.rvelaz.domain.Tweet;
import uk.ac.liv.rvelaz.domain.User;
import uk.ac.liv.rvelaz.domain.repository.TweetRepository;

import java.util.Date;
import java.util.List;

/**
 * User: raul
 * Date: 06/12/2012
 * Time: 13:00
 */
@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration(locations = {"classpath:domain/domain-context.xml"})
@ContextConfiguration(locations = {"classpath:domain/repository/embedded-mongo-context.xml",
        "classpath:domain/repository/repository-config.xml"})
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class TweetRepositoryUnitTest extends AbstactEmbeddedMongo{

    @Autowired
    private TweetRepository tweetRepository;

    @Test
    public void getTweetsForParticipant(){
        Participant participant = new Participant();
        participant.setUserId(1L);
        participant.setScreenName("GoldsmithsUoL");

        Tweet status1 = createTweet("tweet 1", 1L, "user1");
        Tweet status2 = createTweet("tweet 2", 1L, "user1");
        Tweet status3 = createTweet("tweet 3", 1L, "user1");
        Tweet status4 = createTweet("tweet 4", 2L, "user2");
        Tweet status5 = createTweet("tweet 5", 2L, "user2");
        Tweet status6 = createTweet("tweet 6", 3L, "user3");
        Tweet status7 = createTweet("tweet 7", 4L, "user4");
        Tweet status8 = createTweet("tweet 8", 5L, "user5");
        Tweet status9 = createTweet("tweet 9", 5L, "user5");
        Tweet status10 = createTweet("tweet 10", 5L, "user5");
        Tweet status11 = createTweet("tweet 11", 5L, "user5");
        Tweet status12 = createTweet("tweet 12", 1L, "user1");


        tweetRepository.add(status2);
        tweetRepository.add(status1);
        tweetRepository.add(status3);
        tweetRepository.add(status4);
        tweetRepository.add(status5);
        tweetRepository.add(status6);
        tweetRepository.add(status7);
        tweetRepository.add(status8);
        tweetRepository.add(status9);
        tweetRepository.add(status10);
        tweetRepository.add(status11);
        tweetRepository.add(status12);


        List<Tweet> statuses = tweetRepository.findTweets(participant);

        Assert.assertEquals(4, statuses.size());

    }




    private Tweet createTweet(final String text, final Long userId, final String screenName) {
        User user = new User();
        user.setCreatedAt(new Date());
        user.setId(userId);
        user.setScreenName(screenName);

        Tweet tweet = new Tweet();
        tweet.setText(text);
        tweet.setUser(user);
        tweet.setId(Double.doubleToLongBits(Math.random()*100));

        return tweet;
    }

}
