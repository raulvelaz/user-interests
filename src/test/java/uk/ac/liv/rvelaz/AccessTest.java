package uk.ac.liv.rvelaz;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.convert.MongoConverter;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import twitter4j.*;
import twitter4j.auth.AccessToken;
import uk.ac.liv.rvelaz.common.transformer.StatusTransformer;
import uk.ac.liv.rvelaz.common.transformer.UserTransformer;
import uk.ac.liv.rvelaz.domain.Participant;
import uk.ac.liv.rvelaz.domain.repository.ParticipantRepository;
import uk.ac.liv.rvelaz.domain.repository.StatusReadConverter;
import uk.ac.liv.rvelaz.domain.repository.TweetRepository;
import uk.ac.liv.rvelaz.service.FriendRequester;
import uk.ac.liv.rvelaz.service.TweetRequester;

import java.util.List;

/**
 * User: raul
 * Date: 21/11/2012
 * Time: 11:19
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:connector/connector-application-context.xml"})
public class AccessTest {
    @Value("${tokenSecret}")
    private String tokenSecret;

    @Value("${token}")
    private String token;

    @Autowired
    private FriendRequester friendRequester;

    @Autowired
    private TweetRequester tweetRequester;

    @Autowired
    private ParticipantRepository participantRepository;

    @Autowired
    private TweetRepository tweetRepository;



    MongoDbFactory mongoDbFactory;

    MongoConverter mongoConverter;


    @Ignore
    @Test
    public void aa() throws TwitterException {
        // The factory instance is re-useable and thread safe.
        TwitterFactory factory = new TwitterFactory();
        AccessToken accessToken = loadAccessToken();
        Twitter twitter = factory.getInstance();
        twitter.setOAuthConsumer("sX3HuIkJiLbTjjXicPogA", "PjfKw23rjQQfGLdRcDoL0sOI5I7Hd25S1krMFe9tP0");
        twitter.setOAuthAccessToken(accessToken);
        System.out.println(accessToken.getUserId());
        System.out.println(twitter.getRateLimitStatus().getRemainingHits());
        System.out.println(twitter.getRateLimitStatus().getHourlyLimit());
        System.out.println(twitter.getRateLimitStatus().getSecondsUntilReset());
//        IDs iDs = twitter.getFriendsIDs("khamisnaseri", -1);
//        System.out.println(iDs);
//        System.out.println("Friends " + iDs.getIDs().length);

//        System.out.println("Followers " + twitter.getFollowersIDs(60716234, -1).getIDs().length);

//        for (long id : twitter.getFriendsIDs(133644768, -1).getIDs()){
//            System.out.println(id);
//        }

//        List<Status> statuses = twitter.getFriendsTimeline();
//        System.out.println("Showing friends timeline.");
//        for (Status status : statuses) {
//            System.out.println(status.getUser().getName() + ":" +
//                    status.getText());
//        }
        Paging paging = new Paging();
        paging.setCount(200);
        paging.setPage(16);
//        System.out.println("Page before: " + paging.getMaxId());

//        ResponseList<Status> statuses = twitter.getUserTimeline("ZDNet", paging);
//        System.out.println(twitter.getRateLimitStatus().getRemainingHits());
//        ResponseList<Status> statuses = twitter.getUserTimeline("naval", paging);

        Participant participant = new Participant();
//        participant.setScreenName("naval");
//        participant.setUserId(745273L);
        participant.setScreenName("raulvelazm");
        participant.setUserId(133644768L);
        participant.setControlGroup(false);

//        System.out.println("Remaining hits " + twitter.getRateLimitStatus().getRemainingHits());

        List<Status> tweets = tweetRequester.requestTweets(participant);

//        StatusTransformer statusTransformer = new StatusTransformer();

//        for(Status status : tweets){
//            tweetRepository.add(statusTransformer.transform(status));
//        }


//        ResponseList<Status> tweets = twitter.getUserTimeline("naval", paging);

        System.out.println("Received tweets " + tweets.size());
//        System.out.println("Remaining hits " + twitter.getRateLimitStatus().getRemainingHits());

//        for (Status status : tweets) {
//            System.out.println(status.getCreatedAt());
//        }
    }

    @Ignore
    @Test
    public void bb() throws TwitterException {
        TwitterFactory factory = new TwitterFactory();
        AccessToken accessToken = loadAccessToken();
        Twitter twitter = factory.getInstance();
        twitter.setOAuthConsumer("sX3HuIkJiLbTjjXicPogA", "PjfKw23rjQQfGLdRcDoL0sOI5I7Hd25S1krMFe9tP0");
        twitter.setOAuthAccessToken(accessToken);

        long[] users = {44282335L};
        ResponseList<User> responseList = twitter.lookupUsers(users);
        for (User user : responseList) {
            System.out.println("followers count" + user.getFollowersCount());
            System.out.println("statuses count" + user.getStatusesCount());
        }
    }

    private AccessToken loadAccessToken() {
        return new AccessToken(token, tokenSecret);
    }

    @Ignore
    @Test
    public void a() throws TwitterException {
        Participant participant = new Participant();


        participant.setScreenName("raulvelazm");
        participant.setUserId(133644768L);
        participant.setControlGroup(false);
        List<Status> tweets = tweetRequester.requestTweets(participant);

        UserTransformer userTransformer = new UserTransformer();
        StatusTransformer statusTransformer = new StatusTransformer(userTransformer);


        for (Status status : tweets) {
            tweetRepository.add(statusTransformer.transform(status));
        }

        System.out.println("Received tweets " + tweets.size());

    }
    @Ignore
    @Test
    public void bdb() {

        Participant participant = participantRepository.getParticipantById(19117861L);
        int partTweets = tweetRepository.findTweets(participant).size();

        System.out.println("Research participant: " + participant.getScreenName());
        System.out.println("Friends: " + participant.getFriendsIds().size());
        System.out.println("Tweets: " + partTweets);

        List<Participant> participants = participantRepository.getFriendsForParticipant(participant);

        int totalTweets = partTweets;
        for (Participant p : participants) {
            int tw = tweetRepository.findTweets(p).size();
            System.out.println("Friend participant: " + p.getUserId());
            System.out.println("Friend tweets: " + tw);
            totalTweets = totalTweets + tw;

            System.out.println("Sub-total tweets: " + totalTweets);
        }

        System.out.println("Total tweets: " + totalTweets);

    }

}

