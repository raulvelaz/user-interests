package uk.ac.liv.rvelaz.infrastructure.integration;

import com.cybozu.labs.langdetect.LangDetectException;
import org.junit.Before;
import org.junit.Test;
import static org.mockito.Mockito.*;

import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import twitter4j.Status;
import uk.ac.liv.rvelaz.domain.Tweet;

import static org.junit.Assert.*;

/**
 * User: raul
 * Date: 06/12/2012
 * Time: 11:13
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:streaming/streaming-connector-test.xml",
        "classpath:streaming/streaming-context-test.xml","classpath:domain/repository/repository-test.xml"})
public class LanguageFilterUnitTest {

    @Autowired
    private LanguageFilter languageFilter;

    @Test
    public void allowEnglishTweets() throws LangDetectException {
        String text = "this text is in english";
        Tweet status = mock(Tweet.class);
        when(status.getText()).thenReturn(text);

        assertTrue(languageFilter.filter(status));
    }

    @Test
    public void filterOutNonEnglishTweets() throws LangDetectException {
        String text = "este texto esta en castellano";
        Tweet status = mock(Tweet.class);

        when(status.getText()).thenReturn(text);
        assertFalse(languageFilter.filter(status));
    }


}
