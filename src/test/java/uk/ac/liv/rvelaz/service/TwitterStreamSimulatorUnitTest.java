package uk.ac.liv.rvelaz.service;

import com.sun.xml.internal.ws.wsdl.writer.document.Part;
import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.integration.Message;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;
import uk.ac.liv.rvelaz.domain.Experiment;
import uk.ac.liv.rvelaz.domain.Participant;
import uk.ac.liv.rvelaz.domain.Tweet;
import uk.ac.liv.rvelaz.domain.repository.ExperimentRepository;
import uk.ac.liv.rvelaz.domain.repository.ParticipantRepository;
import uk.ac.liv.rvelaz.infrastructure.integration.ConnectorGateway;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.mockito.Mockito.*;

/**
 * User: raul
 * Date: 07/12/2012
 * Time: 08:59
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:streaming/streaming-connector-test.xml", "classpath:streaming/streaming-services-test.xml",
        "classpath:domain/repository/embedded-mongo-context.xml"})
public class TwitterStreamSimulatorUnitTest {

    @Autowired
    @Qualifier("experimentGateway")
    private ConnectorGateway connectorGateway;

    @Autowired
    private ParticipantRepository participantRepository;

    @Autowired
    private ExperimentRepository experimentRepository;

    @Autowired
    private TwitterStreamSimulator twitterStreamSimulator;


    private Participant participant;
    private Participant participant1;
    private Participant participant2;

    @Before
    public void setUp() {
        ReflectionTestUtils.setField(twitterStreamSimulator, "connectorGateway", connectorGateway);
        reset(participantRepository);
        reset(experimentRepository);
        reset(connectorGateway);

        Set<String> topics = new HashSet<String>();
        topics.add("topic1");
        topics.add("topic2");
        List<Long> friendsIds = new ArrayList<Long>();
        friendsIds.add(1L);
        friendsIds.add(2L);
        participant = new Participant();
        participant.setScreenName("joe");
        participant.setControlGroup(false);
        participant.setEmail("email@email.com");
        participant.setFriendsIds(friendsIds);
        participant.setTopics(topics);
        participant.setUserId(10L);

        participant1 = new Participant();
        participant1.setScreenName("testJoe");
        participant1.setControlGroup(false);
        participant1.setEmail("email2@email.com");
        participant1.setFriendsIds(friendsIds);
        Set<String> topics1 = new HashSet<String>();
        topics1.add("no topic");
        participant1.setTopics(topics1);
        participant1.setUserId(1L);

        participant2 = new Participant();
        participant2.setScreenName("testJoe1");
        participant2.setControlGroup(false);
        participant2.setEmail("email33@email.com");
        participant2.setFriendsIds(friendsIds);
        participant2.setTopics(topics1);
        participant2.setUserId(2L);
    }


    @Test
    public void startExperimentForTestUserWhenThereAreNoExperiments() throws IOException {
        List<Participant> participants = createTestParticipants();

        when(participantRepository.getTestParticipants()).thenReturn(participants);
        when(experimentRepository.hasExperiment(participant)).thenReturn(false);
        when(experimentRepository.hasExperiment(participant1)).thenReturn(false);

        twitterStreamSimulator.simulateStream();

        verify(participantRepository, times(1)).getTestParticipants();
        verify(experimentRepository, times(2)).hasExperiment(any(Participant.class));
        verify(connectorGateway, times(2)).sendParticipant(any(Participant.class));
    }

    @Test
    public void startExperimentForTestUserWhenUserHaveExperiments() throws IOException {
        List<Participant> participants = createTestParticipants();

        when(participantRepository.getTestParticipants()).thenReturn(participants);
        when(experimentRepository.hasExperiment(participant)).thenReturn(false);
        when(experimentRepository.hasExperiment(participant1)).thenReturn(true);

        twitterStreamSimulator.simulateStream();


        verify(participantRepository, times(1)).getTestParticipants();
        verify(experimentRepository, times(2)).hasExperiment(any(Participant.class));
        verify(connectorGateway, times(1)).sendParticipant(any(Participant.class));

    }


    private List<Participant> createTestParticipants() {
        List<Participant> testParticipants = new ArrayList<Participant>();

        testParticipants.add(participant);
        testParticipants.add(participant1);

        return testParticipants;

    }

}
