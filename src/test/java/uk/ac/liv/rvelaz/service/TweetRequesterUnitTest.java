package uk.ac.liv.rvelaz.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;
import twitter4j.*;
import uk.ac.liv.rvelaz.domain.Participant;
import uk.ac.liv.rvelaz.domain.ParticipantFriend;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;

/**
 * User: raul
 * Date: 21/11/2012
 * Time: 16:19
 */

@RunWith(MockitoJUnitRunner.class)
public class TweetRequesterUnitTest {
    private Twitter twitter;

    private TweetRequester tweetRequester;


    @Before
    public void setUp() {
        twitter = mock(Twitter.class);

        tweetRequester = new TweetRequester();
        ReflectionTestUtils.setField(tweetRequester, "twitter", twitter);

        reset(twitter);
    }

    @Test
    public void returnedLessTweetsThanMaximumNumberRateLimitNotExceededMakeRequest() throws TwitterException {
        int maxNumTweets = 200;
        String screenName = "user";
        Long userId = 12L;

        Participant mockParticipant = new Participant();
        mockParticipant.setScreenName(screenName);
        mockParticipant.setUserId(userId);

        RateLimitStatus rateLimitStatus = mock(RateLimitStatus.class);

        when(rateLimitStatus.getRemainingHits()).thenReturn(20);
        when(twitter.getRateLimitStatus()).thenReturn(rateLimitStatus);


        Paging paging = new Paging();
        paging.setCount(maxNumTweets);
        paging.setPage(1);
        ResponseList<Status> statuses = mock(ResponseList.class);

        when(statuses.size()).thenReturn(150);
        Status[] statusArray = new Status[150];
        when(statuses.toArray()).thenReturn(statusArray);


        when(twitter.getUserTimeline(userId, paging)).thenReturn(statuses);

        List<Status> receivedStatuses = tweetRequester.requestTweets(mockParticipant);


        verify(rateLimitStatus, times(1)).getRemainingHits();
        verify(rateLimitStatus, times(2)).getSecondsUntilReset();
        verify(twitter, times(1)).getUserTimeline(userId, paging);

        assertEquals(150, receivedStatuses.size());

    }


    @Test
    public void returnedMoreOrEqualTweetsThanMaximumNumberRateLimitNotExceededMakeRequest() throws TwitterException {

        int maxNumTweets = 200;
        String screenName = "user";
        long maxId = 1242342L;
        Long userId = 12L;

        Participant mockParticipant = new Participant();
        mockParticipant.setScreenName(screenName);
        mockParticipant.setUserId(userId);

        RateLimitStatus rateLimitStatus = mock(RateLimitStatus.class);

        when(rateLimitStatus.getRemainingHits()).thenReturn(20);
        when(twitter.getRateLimitStatus()).thenReturn(rateLimitStatus);


        Paging paging = new Paging();
        paging.setCount(maxNumTweets);
        paging.setPage(1);
        ResponseList<Status> statuses1stSet = mock(ResponseList.class);
        Status[] status1Array = new Status[200];
        when(statuses1stSet.size()).thenReturn(200);
        when(statuses1stSet.toArray()).thenReturn(status1Array);

        Paging paging1 = new Paging();
        paging1.setCount(maxNumTweets);
//        paging1.setMaxId(maxId);
        paging1.setPage(2);
        ResponseList<Status> statuses2ndSet = mock(ResponseList.class);
        Status[] status2Array = new Status[150];
        when(statuses2ndSet.size()).thenReturn(150);
        when(statuses2ndSet.toArray()).thenReturn(status2Array);

        when(twitter.getUserTimeline(userId, paging)).thenReturn(statuses1stSet);

        when(twitter.getUserTimeline(userId, paging1)).thenReturn(statuses2ndSet);

//        when(paginationService.findMinimumTweetId(statuses1stSet)).thenReturn(maxId);

        List<Status> receivedStatuses = tweetRequester.requestTweets(mockParticipant);


        verify(rateLimitStatus, times(2)).getRemainingHits();
        verify(rateLimitStatus, times(4)).getSecondsUntilReset();

        verify(twitter, times(1)).getUserTimeline(userId, paging);
//        verify(paginationService, times(1)).findMinimumTweetId(statuses1stSet);


        verify(twitter, times(1)).getUserTimeline(userId, paging1);


        //the tweet with id = maxId will be returned twice
        assertEquals(350, receivedStatuses.size());

    }


}
