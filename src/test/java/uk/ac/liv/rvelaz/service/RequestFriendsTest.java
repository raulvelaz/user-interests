package uk.ac.liv.rvelaz.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import uk.ac.liv.rvelaz.domain.Participant;
import uk.ac.liv.rvelaz.infrastructure.integration.ConnectorGateway;
import uk.ac.liv.rvelaz.common.to.ParticipantTO;
import uk.ac.liv.rvelaz.common.transformer.CSVTransformer;
import uk.ac.liv.rvelaz.common.transformer.ParticipantTransformer;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

/**
 * User: raul
 * Date: 21/11/2012
 * Time: 13:51
 */

@RunWith(MockitoJUnitRunner.class)
public class RequestFriendsTest {
    @Mock
    private CSVTransformer csvTransformer;

    @Mock
    private ParticipantTransformer participantTransformer;

    @Mock
    private ConnectorGateway connectorGateway;

    @InjectMocks
    private RequestFriends requestFriends;

    @Test
    public void sendGetFriendRequestToRateLimiter(){
        ParticipantTO participantTO1 = mock(ParticipantTO.class);
        ParticipantTO participantTO2 = mock(ParticipantTO.class);
        ParticipantTO participantTO3 = mock(ParticipantTO.class);
        ParticipantTO participantTO4 = mock(ParticipantTO.class);
        ParticipantTO participantTO5 = mock(ParticipantTO.class);

        List<ParticipantTO> participants = new ArrayList<ParticipantTO>();
        participants.add(participantTO1);
        participants.add(participantTO2);
        participants.add(participantTO3);
        participants.add(participantTO4);
        participants.add(participantTO5);


        Participant participant = mock(Participant.class);

        when(csvTransformer.getParticipants()).thenReturn(participants);

        when(participantTransformer.transformTO(any(ParticipantTO.class))).thenReturn(participant);

        requestFriends.requestFriendsForParticipants();

        verify(participantTransformer, times(5)).transformTO(any(ParticipantTO.class));
        verify(connectorGateway, times(5)).sendParticipant(any(Participant.class));
    }

}
