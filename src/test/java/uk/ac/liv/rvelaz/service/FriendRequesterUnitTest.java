package uk.ac.liv.rvelaz.service;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;
import twitter4j.*;
import uk.ac.liv.rvelaz.domain.Participant;
import uk.ac.liv.rvelaz.domain.ParticipantFriend;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;

/**
 * User: raul
 * Date: 21/11/2012
 * Time: 16:19
 */

@RunWith(MockitoJUnitRunner.class)
public class FriendRequesterUnitTest {
    private Twitter twitter;

    private FriendRequester friendRequester;


    @Before
    public void setUp() {
        twitter = mock(Twitter.class);

        friendRequester = new FriendRequester();
        ReflectionTestUtils.setField(friendRequester, "twitter", twitter);

        reset(twitter);
    }

    @Test
    public void requestFriendsRateLimitNotExceededMakeRequest() throws TwitterException {
        Participant mockParticipant = new Participant();
        mockParticipant.setUserId(10L);
        mockParticipant.setScreenName("screenName");
        IDs iDs = mock(IDs.class);
        long[] ids = {1L, 2L, 3L};

        RateLimitStatus rateLimitStatus = mock(RateLimitStatus.class);

        User participant = mock(User.class);
        when(participant.getId()).thenReturn(40L);


        User user = mock(User.class);
        when(user.getStatusesCount()).thenReturn(40);
        when(user.getFollowersCount()).thenReturn(300);

        User friend1 = mock(User.class);
        when(friend1.getStatusesCount()).thenReturn(4660);
        when(friend1.getFollowersCount()).thenReturn(30);

        User friend2 = mock(User.class);
        when(friend2.getStatusesCount()).thenReturn(50);
        when(friend2.getFollowersCount()).thenReturn(100);

        User friend3 = mock(User.class);
        when(friend3.getStatusesCount()).thenReturn(20);
        when(friend3.getFollowersCount()).thenReturn(2200);

        ResponseList<User> participantData = mock(ResponseList.class);
        when(participantData.get(0)).thenReturn(participant);

        ResponseList<User> users = mock(ResponseList.class);
        when(users.get(0)).thenReturn(user);
        when(users.get(1)).thenReturn(friend1);
        when(users.get(2)).thenReturn(friend2);
        when(users.get(3)).thenReturn(friend3);
        when(users.size()).thenReturn(4);


        when(rateLimitStatus.getRemainingHits()).thenReturn(20);
        when(twitter.getRateLimitStatus()).thenReturn(rateLimitStatus);

        when(iDs.getIDs()).thenReturn(ids);
        when(iDs.getNextCursor()).thenReturn(0L);

        when(twitter.getFriendsIDs(anyString(), anyLong())).thenReturn(iDs);

        when(twitter.lookupUsers(Matchers.<String[]>anyObject())).thenReturn(participantData);

        when(twitter.lookupUsers(Matchers.<long[]>anyObject())).thenReturn(users);


        List<Participant> participants = friendRequester.requestFriends(mockParticipant);

        verify(twitter, times(1)).getRateLimitStatus();
        verify(rateLimitStatus, times(1)).getRemainingHits();
        verify(rateLimitStatus, times(2)).getSecondsUntilReset();


        assertEquals(4, participants.size());
        assertEquals(3, participants.get(0).getFriendsIds().size());
        assertTrue(participants.get(1) instanceof ParticipantFriend);

    }


    @Test
    public void requestFriendsRateLimitNotExceededPaginateRequest() throws TwitterException {
        Participant mockParticipant = new Participant();
        mockParticipant.setScreenName("username");
        IDs iDs = mock(IDs.class);
        IDs iDs1 = mock(IDs.class);

        long[] ids = {1L, 2L, 3L};
        long[] ids1 = {1L, 2L, 3L};
        Long userId = 100L;
        Long nextCursor = 3L;

        User participant = mock(User.class);
        when(participant.getId()).thenReturn(40L);

        User user = mock(User.class);
        when(user.getStatusesCount()).thenReturn(40);
        when(user.getFollowersCount()).thenReturn(300);

        User friend1 = mock(User.class);
        when(friend1.getStatusesCount()).thenReturn(4660);
        when(friend1.getFollowersCount()).thenReturn(30);

        User friend2 = mock(User.class);
        when(friend2.getStatusesCount()).thenReturn(50);
        when(friend2.getFollowersCount()).thenReturn(100);

        User friend3 = mock(User.class);
        when(friend3.getStatusesCount()).thenReturn(20);
        when(friend3.getFollowersCount()).thenReturn(2200);

        ResponseList<User> participantData = mock(ResponseList.class);
        when(participantData.get(0)).thenReturn(participant);

        ResponseList<User> users = mock(ResponseList.class);
        when(users.get(0)).thenReturn(user);
        when(users.get(1)).thenReturn(friend1);
        when(users.get(2)).thenReturn(friend2);
        when(users.get(3)).thenReturn(friend3);
        when(users.size()).thenReturn(4);


        RateLimitStatus rateLimitStatus = mock(RateLimitStatus.class);

        when(rateLimitStatus.getRemainingHits()).thenReturn(20);
        when(twitter.getRateLimitStatus()).thenReturn(rateLimitStatus);

        when(twitter.getId()).thenReturn(userId);

        when(iDs.getIDs()).thenReturn(ids);
        when(iDs.getNextCursor()).thenReturn(nextCursor);

        when(iDs1.getIDs()).thenReturn(ids1);
        when(iDs1.getNextCursor()).thenReturn(0L);

        when(twitter.getFriendsIDs(mockParticipant.getScreenName(), -1)).thenReturn(iDs);
        when(twitter.getFriendsIDs(mockParticipant.getScreenName(), nextCursor)).thenReturn(iDs1);

        when(twitter.lookupUsers(Matchers.<String[]>anyObject())).thenReturn(participantData);

        when(twitter.lookupUsers(Matchers.<long[]>anyObject())).thenReturn(users);


        List<Participant> participants = friendRequester.requestFriends(mockParticipant);

        verify(twitter, times(2)).getRateLimitStatus();
        verify(rateLimitStatus, times(2)).getRemainingHits();
        verify(rateLimitStatus, times(4)).getSecondsUntilReset();


        assertEquals(6, participants.get(0).getFriendsIds().size());

    }

    @Ignore
    @Test
    public void rateLimitExceededWaitUntilTimeLimitOff() throws TwitterException {
        Participant mockParticipant = new Participant();
        mockParticipant.setScreenName("username");
        IDs iDs = mock(IDs.class);
        long[] ids = {1L, 2L, 3L};

        RateLimitStatus rateLimitStatus = mock(RateLimitStatus.class);

        User participant = mock(User.class);
        when(participant.getId()).thenReturn(40L);
        ResponseList<User> participantData = mock(ResponseList.class);
        when(participantData.get(0)).thenReturn(participant);

        User user = mock(User.class);
        when(user.getStatusesCount()).thenReturn(40);
        when(user.getFollowersCount()).thenReturn(300);

        User friend1 = mock(User.class);
        when(friend1.getStatusesCount()).thenReturn(4660);
        when(friend1.getFollowersCount()).thenReturn(30);

        User friend2 = mock(User.class);
        when(friend2.getStatusesCount()).thenReturn(50);
        when(friend2.getFollowersCount()).thenReturn(100);

        User friend3 = mock(User.class);
        when(friend3.getStatusesCount()).thenReturn(20);
        when(friend3.getFollowersCount()).thenReturn(2200);


        ResponseList<User> users = mock(ResponseList.class);
        when(users.get(0)).thenReturn(user);
        when(users.get(1)).thenReturn(friend1);
        when(users.get(2)).thenReturn(friend2);
        when(users.get(3)).thenReturn(friend3);
        when(users.size()).thenReturn(4);

        when(rateLimitStatus.getRemainingHits()).thenReturn(0);
        when(rateLimitStatus.getSecondsUntilReset()).thenReturn(1);
        when(twitter.getRateLimitStatus()).thenReturn(rateLimitStatus);

        when(iDs.getIDs()).thenReturn(ids);

        when(twitter.getFriendsIDs(anyString(), anyLong())).thenReturn(iDs);

        when(twitter.lookupUsers(Matchers.<String[]>anyObject())).thenReturn(participantData);


        when(twitter.lookupUsers(Matchers.<long[]>anyObject())).thenReturn(users);

        List<Participant> participants = friendRequester.requestFriends(mockParticipant);

        verify(twitter, times(1)).getRateLimitStatus();
        verify(rateLimitStatus, times(1)).getRemainingHits();
        verify(rateLimitStatus, times(1)).getSecondsUntilReset();


        assertEquals(3, participants.get(0).getFriendsIds().size());

    }


}
