package uk.ac.liv.rvelaz.service;

import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.Message;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;
import uk.ac.liv.rvelaz.clustering.ICluster;
import uk.ac.liv.rvelaz.domain.Experiment;
import uk.ac.liv.rvelaz.domain.Participant;
import uk.ac.liv.rvelaz.domain.Tweet;
import uk.ac.liv.rvelaz.domain.repository.ExperimentRepository;
import uk.ac.liv.rvelaz.domain.repository.ParticipantRepository;

import java.io.IOException;

import static org.mockito.Mockito.*;

/**
 * User: raul
 * Date: 07/12/2012
 * Time: 12:37
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:streaming/streaming-connector-test.xml", "classpath:streaming/streaming-services-test.xml",
        "classpath:domain/repository/embedded-mongo-context.xml"}
)
public class ExperimentRunnerUnitTest {

    private ICluster clusteringAlgorithm;

    private ExperimentRunner experimentRunner;

    @Autowired
    private ParticipantRepository participantRepository;
    @Autowired
    private ExperimentRepository experimentRepository;;

    @Before
    public void setUp() {
        clusteringAlgorithm = mock(ICluster.class);
        experimentRunner = new ExperimentRunner(participantRepository, experimentRepository);
        ReflectionTestUtils.setField(experimentRunner, "clusteringAlgorithm", clusteringAlgorithm);
        ReflectionTestUtils.setField(experimentRunner, "participantRepository", participantRepository);
    }

    @Test
    public void startNewExperimentWhenThereIsANewParticipant() throws IOException {
        Tweet tweet = new Tweet();
        tweet.setText("tweet text text");
        tweet.setId(10L);

        Participant participant = new Participant();
        participant.setUserId(1L);
        participant.setControlGroup(false);
        participant.setScreenName("screenName");


        Message<Tweet> message = MessageBuilder.withPayload(tweet)
                .setHeader("participant", participant.getUserId())
                .build();

        when(participantRepository.getParticipantById(1L)).thenReturn(participant);

        experimentRunner.startOrContinueExperiment(message);

        verify(participantRepository, times(1)).getParticipantById(participant.getUserId());
        verify(clusteringAlgorithm, times(1)).startNewExperiment(any(Experiment.class), eq(message.getPayload()));

        Assert.assertEquals(1, experimentRunner.getExperimentMap().get(participant.getUserId()).getNumberOfTweets());
        Assert.assertEquals("screenName", experimentRunner.getExperimentMap().get(participant.getUserId()).getParticipant().getScreenName());
    }

    @Test
    public void continueWithExperimentIfTweetsForSameParticipantArrive() throws IOException {
        Tweet tweet = new Tweet();
        tweet.setText("tweet text text");
        tweet.setId(10L);

        Tweet tweet1 = new Tweet();
        tweet.setText("tweet text text");
        tweet.setId(11L);

        Participant participant = new Participant();
        participant.setUserId(1L);
        participant.setControlGroup(false);
        participant.setScreenName("screenName");


        Message<Tweet> message = MessageBuilder.withPayload(tweet)
                .setHeader("participant", participant.getUserId())
                .build();

        Message<Tweet> message1 = MessageBuilder.withPayload(tweet1)
                .setHeader("participant", participant.getUserId())
                .build();

        experimentRunner.startOrContinueExperiment(message);
        experimentRunner.startOrContinueExperiment(message1);

        verify(clusteringAlgorithm, times(1)).startNewExperiment(any(Experiment.class), eq(message.getPayload()));
        verify(clusteringAlgorithm, times(1)).cluster(tweet1);

        Assert.assertEquals(2, experimentRunner.getExperimentMap().get(participant.getUserId()).getNumberOfTweets());

    }

    @Test
    public void startNewExperimentIfTweetsForDifferentParticipantArrives() throws IOException {
        Tweet tweet = new Tweet();
        tweet.setText("tweet text text");
        tweet.setId(10L);

        Tweet tweet1 = new Tweet();
        tweet.setText("tweet text text");
        tweet.setId(11L);

        Tweet tweet2 = new Tweet();
        tweet.setText("tweet text text");
        tweet.setId(12L);

        Participant participant = new Participant();
        participant.setUserId(1L);
        participant.setControlGroup(false);
        participant.setScreenName("screenName");

        when(participantRepository.getParticipantById(1L)).thenReturn(participant);

        Participant participant1 = new Participant();
        participant1.setUserId(2L);
        participant1.setControlGroup(false);
        participant1.setScreenName("screenName1");

        when(participantRepository.getParticipantById(2L)).thenReturn(participant1);

        Message<Tweet> message = MessageBuilder.withPayload(tweet)
                .setHeader("participant", participant.getUserId())
                .build();

        Message<Tweet> message1 = MessageBuilder.withPayload(tweet1)
                .setHeader("participant", participant.getUserId())
                .build();

        Message<Tweet> message2 = MessageBuilder.withPayload(tweet2)
                .setHeader("participant", participant1.getUserId())
                .build();

        experimentRunner.startOrContinueExperiment(message);
        experimentRunner.startOrContinueExperiment(message1);
        experimentRunner.startOrContinueExperiment(message2);

        verify(clusteringAlgorithm, times(1)).startNewExperiment(any(Experiment.class), eq(message.getPayload()));
        verify(clusteringAlgorithm, times(1)).cluster(tweet1);
        verify(clusteringAlgorithm, times(1)).endExperiment(eq(message2.getPayload()));

        Assert.assertEquals(2, experimentRunner.getExperimentMap().get(participant.getUserId()).getNumberOfTweets());
        Assert.assertEquals(1, experimentRunner.getExperimentMap().get(participant1.getUserId()).getNumberOfTweets());


    }

}
