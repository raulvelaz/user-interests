package uk.ac.liv.rvelaz.service;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.Message;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import uk.ac.liv.rvelaz.domain.Participant;
import uk.ac.liv.rvelaz.domain.ParticipantFriend;
import uk.ac.liv.rvelaz.domain.Tweet;
import uk.ac.liv.rvelaz.domain.repository.ParticipantRepository;
import uk.ac.liv.rvelaz.domain.repository.TweetRepository;

import static org.mockito.Mockito.*;

import java.util.*;

/**
 * User: raul
 * Date: 06/12/2012
 * Time: 12:19
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:streaming/streaming-connector-test.xml","classpath:streaming/streaming-services-test.xml",
        "classpath:domain/repository/embedded-mongo-context.xml"})
public class TweetCorpusFinderUnitTest {

    @Autowired
    private TweetRepository tweetRepository;

    @Autowired
    private ParticipantRepository participantRepository;

    private TweetCorpusFinder tweetCorpusFinder;

    @Before
    public void setUp(){
        tweetCorpusFinder = new TweetCorpusFinder(participantRepository, tweetRepository);
    }

    @Test
    public void getAllTweetsFromAParticipantAndFriends() {
        long participant1FriendId = 1L;
        long participant2FriendId = 2L;
        List<Long> friendsIds = new ArrayList<Long>();
        friendsIds.add(participant1FriendId);
        friendsIds.add(participant2FriendId);

        Participant participant =  mock(Participant.class);
        when(participant.getFriendsIds()).thenReturn(friendsIds);

        ParticipantFriend participantFriend1 = mock(ParticipantFriend.class);
        ParticipantFriend participantFriend2 = mock(ParticipantFriend.class);

        Tweet statusParticipant1 = mock(Tweet.class);
        Tweet statusParticipant2 = mock(Tweet.class);
        List<Tweet> statusListParticipant1 = new ArrayList<Tweet>();
        statusListParticipant1.add(statusParticipant1);
        statusListParticipant1.add(statusParticipant2);


        Tweet statusFriend1Participant1 = mock(Tweet.class);
        Tweet statusFriend1Participant2 = mock(Tweet.class);
        Tweet statusFriend1Participant3 = mock(Tweet.class);

        List<Tweet> statusListFriend1 = new ArrayList<Tweet>();
        statusListFriend1.add(statusFriend1Participant1);
        statusListFriend1.add(statusFriend1Participant2);
        statusListFriend1.add(statusFriend1Participant3);

        Tweet statusFriend2Participant1 = mock(Tweet.class);
        Tweet statusFriend2Participant2 = mock(Tweet.class);
        Tweet statusFriend2Participant3 = mock(Tweet.class);
        Tweet statusFriend2Participant4 = mock(Tweet.class);

        List<Tweet> statusListFriend2 = new ArrayList<Tweet>();
        statusListFriend2.add(statusFriend2Participant1);
        statusListFriend2.add(statusFriend2Participant2);
        statusListFriend2.add(statusFriend2Participant3);
        statusListFriend2.add(statusFriend2Participant4);



        List<Participant> allFriends = new ArrayList<Participant>();
        allFriends.add(participantFriend1);
        allFriends.add(participantFriend2);

        when(participantRepository.getFriendsForParticipant(participant)).thenReturn(allFriends);
        when(tweetRepository.findTweets(participant)).thenReturn(statusListParticipant1);
        when(tweetRepository.findTweets(participantFriend1)).thenReturn(statusListFriend1);
        when(tweetRepository.findTweets(participantFriend2)).thenReturn(statusListFriend2);

        Message<List<Tweet>> tweets = tweetCorpusFinder.getTweets(participant);

        verify(participantRepository, times(1)).getFriendsForParticipant(participant);
        verify(tweetRepository, times(3)).findTweets(any(Participant.class));

        assertEquals(9, tweets.getPayload().size());

    }

}
