package uk.ac.liv.rvelaz.common.transformer;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import uk.ac.liv.rvelaz.common.to.ParticipantTO;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.*;

/**
 * User: raul
 * Date: 21/11/2012
 * Time: 12:26
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:connector/connector-application-context-test.xml",
        "classpath:domain/repository/embedded-mongo-context.xml"})
public class CSVTransformerUnitTest {

    @Autowired
    private CSVTransformer csvTransformer;

    @Test
    public void getUsersFromListFile() {
        ParticipantTO participantTO = new ParticipantTO();
        participantTO.setEmail("test@test.com");
        participantTO.setControlGroup(false);
        participantTO.setScreenName("screen_test");
        Set<String> topics = new HashSet<String> ();
        topics.add("software");
        topics.add("cooking");
        participantTO.setTopics(topics);

        List<ParticipantTO> participantsCsv = csvTransformer.getParticipants();

        assertNotNull(participantsCsv.size());
        assertEquals(4, participantsCsv.size());

        assertEquals(participantTO.getEmail(), participantsCsv.get(0).getEmail());
        assertEquals(participantTO.isControlGroup(), participantsCsv.get(0).isControlGroup());
        assertEquals(participantTO.getScreenName(), participantsCsv.get(0).getScreenName());
        assertEquals(participantTO.getTopics().size(), participantsCsv.get(0).getTopics().size());

    }

}
