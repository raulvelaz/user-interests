package uk.ac.liv.rvelaz.clustering.moa;

import com.cybozu.labs.langdetect.LangDetectException;
import com.sun.org.apache.xerces.internal.impl.xpath.regex.Match;
import moa.cluster.CFCluster;
import moa.cluster.Cluster;
import moa.cluster.Clustering;
import moa.clusterers.Clusterer;
import moa.clusterers.CobWeb;
import moa.clusterers.clustream.Clustream;
import moa.clusterers.clustree.ClusKernel;
import moa.clusterers.clustree.ClusTree;
import moa.clusterers.denstream.DenStream;
import moa.clusterers.streamkm.StreamKM;
import moa.core.InstancesHeader;
import moa.core.ObjectRepository;
import moa.gui.visualization.DataPoint;
import moa.options.*;
import moa.tasks.NullMonitor;
import moa.tasks.TaskMonitor;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.Message;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import uk.ac.liv.rvelaz.clustering.ICluster;
import uk.ac.liv.rvelaz.clustering.moa.sketch.*;
import uk.ac.liv.rvelaz.domain.Participant;
import uk.ac.liv.rvelaz.domain.Tweet;
import uk.ac.liv.rvelaz.domain.repository.ClusterRepository;
import uk.ac.liv.rvelaz.domain.repository.ParticipantRepository;
import uk.ac.liv.rvelaz.domain.repository.TweetRepository;
import uk.ac.liv.rvelaz.infrastructure.integration.LanguageFilter;
import uk.ac.liv.rvelaz.service.ExperimentRunner;
import uk.ac.liv.rvelaz.service.TweetCorpusFinder;
import uk.ac.liv.rvelaz.service.TwitterStreamSimulator;
import weka.clusterers.Cobweb;
import weka.clusterers.EM;
import weka.core.Attribute;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.ArffLoader;

import java.io.*;
import java.util.*;

/**
 * User: raul
 * Date: 07/12/2012
 * <p/>
 * Time: 19:14
 * <p/>
 * <p/>
 * <p/>
 * todo: run experiment with decay 150K
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:connector/connector-application-context.xml",
                                    "classpath:streaming/streaming-context.xml"})
public class FilterTfIdfUnitTest extends AbstractOptionHandler {
    int numAtts = 3000;
    LinkedList<DataPoint> pointBuffer0 = new LinkedList<DataPoint>();
    ArrayList<DataPoint> pointarray0 = null;
    int timestamp = 0;
    int decayHorizon = 10000;

    int macroNumber = 0;

    private Clustering microAll = new Clustering();
    private LinkedList<DataPoint> allPoints = new LinkedList<DataPoint>();

    @Autowired
    private LanguageFilter languageFilter;

    @Autowired
    private TweetCorpusFinder tweetCorpusFinder;

    @Autowired
    private ParticipantRepository participantRepository;

    @Autowired
    private TweetRepository tweetRepository;


    @Autowired
    private ClusterRepository clusterRepository;

    @Autowired
    private ExperimentRunner experimentRunner;

    private Clustering macro;


    @Autowired
    private TwitterStreamSimulator twitterStreamSimulator;

    @Ignore
    @Test
    public void test() throws Exception {
//        String text = "MOA (Massive On-line Analysis) is a framework for data stream mining. It includes tools for evaluation and a collection of machine learning algorithms. Related to the WEKA project it is also written in Java while scaling to more demanding problems. The goal of MOA is a benchmark framework for running experiments in the data stream mining context by proving " +
//                "storable settings for data stream (real and synthetic) for repeatable experiments " +
//                "a set of existing algorithms and measures form the literature for comparison and " +
//                "an easily extendable framework for new stream algorithms and evaluation methods. " +
//                "Using MOA " +
//                "The workflow in MOA the simple schema depicted below: first a data stream (feed generator) is chosen and configured second an algorithm (e.g. a classifier) is chosen and its paramters are set third the evaluation method or measure is chosen and finally the results are obtained after running the task, H";

        String t = "The workflow in MOA the simple schema depicted below: first a data stream (feed generator) is schema chosen " +
                "and configured second schema an algorithm (e.g. a classifier) is chosen and schema its paramters are set third the " +
                "evaluation method or measure is schema chosen and finally the results are obtained after running the schema task";

        String t1 = "The workflow second in MOA the second simple schema second depicted second belowschema: first a data stream (feed generator) is chosen " +
                "and schema configured second an algorithm second (e.g. a classifier) is chosen and its paramters schema are set third the " +
                "evaluation method schema or measure is chosen and second finally the results are obtained after running schema the task";


        String text = "MOA (Massive On-line Analysis) is a framework for data stream mining. It includes tools for evaluation and a collection of machine learning algorithms. Related to the WEKA project it is also written in Java while scaling to more demanding problems. The goal of MOA is a benchmark framework for running experiments in the data stream mining context by proving " +
                "storable settings for data stream (real and synthetic) for repeatable experiments " +
                "a set of existing algorithms and measures form the literature for comparison and ";

        ArrayList<Attribute> attributes = new ArrayList<Attribute>(numAtts + 1);

        ArrayList<String> classVal = new ArrayList<String>();
        classVal.add("cluster");
        Attribute classAtt = new Attribute("instance_class", classVal);
        attributes.add(classAtt);

        Instances instances = new Instances("sparseInstances", attributes, numAtts + 1);
        InstancesHeader instancesHeader = new InstancesHeader(instances);
        instancesHeader.setClassIndex(0);


        SpaceSaving sketch = new SpaceSaving();
//        LRUSketch sketch = new LRUSketch();
        sketch.prepareForUse();

        FilterTfIdf filterTfIdf = new FilterTfIdf(sketch, numAtts);


//        Participant participant = participantRepository.getParticipantById(38263144L);
        //shalviec: "musicians", "celebrities", "comics"
//        Participant participant = participantRepository.getParticipantById(367574327L);
//        Participant participant = participantRepository.getParticipantById(19117861L);

        //
//        Participant participant = participantRepository.getParticipantById(75655723L);
        //Mguirgui: politics","news"
//        Participant participant = participantRepository.getParticipantById(20471043L);


        //61299563: kmkteachgeek
        Participant participant = participantRepository.getParticipantById(61299563L);
        List<Tweet> partTweets = tweetRepository.findTweets(participant);


//        Clustream clustream = new Clustream();
//        CobWeb clustream = new CobWeb();
        ClusTree clustream = new ClusTree();
        clustream.prepareForUse();
        clustream.setModelContext(instancesHeader);

        for (Tweet tweet : partTweets) {
            timestamp++;
            Instance instance = filterTfIdf.filter(tweet.getText(), instancesHeader, numAtts);
            DataPoint dataPoint = new DataPoint(instance, timestamp);
            pointBuffer0.add(dataPoint);
            if (pointBuffer0.size() > decayHorizon)
                pointBuffer0.removeFirst();

            clustream.trainOnInstanceImpl(instance);
            //            System.out.println("Instance clustered");
        }

        pointarray0 = new ArrayList<DataPoint>(pointBuffer0);

        System.out.println("Participant tweets clustered. " + partTweets.size() + " tweets.");
        if (clustream.getMicroClusteringResult() != null)
            System.out.println("Micro-clustering dimension: " + clustream.getMicroClusteringResult().size());


        List<Participant> participants = participantRepository.getFriendsForParticipant(participant);

        Instance instance = null;
        int totalTweets = partTweets.size();
        for (Participant p : participants) {
            timestamp++;
            List<Tweet> tw = tweetRepository.findTweets(p);
            if (tw.size() > 0) {
                for (Tweet tweet : tw) {
                    instance = filterTfIdf.filter(tweet.getText(), instancesHeader, numAtts);
                    DataPoint dataPoint = new DataPoint(instance, timestamp);
                    pointBuffer0.add(dataPoint);
                    if (pointBuffer0.size() > decayHorizon)
                        pointBuffer0.removeFirst();

                    clustream.trainOnInstanceImpl(instance);
                }

                pointarray0 = new ArrayList<DataPoint>(pointBuffer0);

                if (instance.dataset().numAttributes() > numAtts)
                    processMicroClusters1(clustream.getMicroClusteringResult(), pointarray0, instance);

                System.out.println(tw.size() + " tweets for participant: " + p.getScreenName() + " ID " + p.getUserId());
                totalTweets = totalTweets + tw.size();
            } else {
                System.out.println("No tweets for participant: " + p.getScreenName() + " ID " + p.getUserId());
            }
        }

        System.out.println("Total tweets: " + totalTweets);

        processMacro(instance);

    }


    @Override
    protected void prepareForUseImpl(TaskMonitor monitor, ObjectRepository repository) {
        prepareForUse(new NullMonitor(), null);
    }

    @Override
    public void getDescription(StringBuilder sb, int indent) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Ignore
    @Test
    public void testtt(){
      twitterStreamSimulator.simulateStream();
    }

//    @Ignore
    @Test
    public void uie() throws IOException, LangDetectException {
        //769289: kitty
        //126999494: dody81180
        // 21860412: Cemvedatisik
        //165398119: reubenmallaby
        //28103490: elwell2000
        Participant participant = participantRepository.getParticipantById(28103490L);
//        List<Tweet> tweetList = tweetRepository.findTweets(participant);
//         Tweet lt1 = null;


        System.out.println("About to get tweets");
        Message<List<Tweet>> message = tweetCorpusFinder.getTweets(participant);
        System.out.println("Tweets retrieved: " + message.getPayload().size());

        //61299563L
        List<Tweet> tweets = message.getPayload();
        Tweet lt = null;
        for (Tweet tweet : tweets) {
            if(languageFilter.filter(tweet)){
                Message<Tweet> tm = MessageBuilder.withPayload(tweet).setHeader("participant", 28103490).build();
                experimentRunner.startOrContinueExperiment(tm);
            }
            lt = tweet;
        }
        Message<Tweet> tm = MessageBuilder.withPayload(lt).setHeader("participant", 231).build();
        experimentRunner.startOrContinueExperiment(tm);

    }

    private Clustering processMicroClusters(Clustering clustering, Instance instance) {

        System.out.println("Final clustering size: " + clustering.size());

        Cluster[] clusters = new Cluster[6];
        Clustering gtClust = null;
        if (clustering.getClustering().size() > 6) {
            for (int k = 0; k < 6; k++) {
                int rand = (int) Math.random() * clustering.size();
                if (clusters[k] != null) {
                    clusters[k] = clustering.get(rand);
                } else {
                    k--;
                }
            }
        } else {
            clusters = new Cluster[clustering.getClustering().size()];
            for (int k = 0; k < clustering.getClustering().size(); k++) {
                clusters[k] = clustering.get(k);
            }
        }


//        for (int i = 0; i < clusters.length; i++){
//           clusterRepository.add((ClusKernel) clusters[i]);
//        }


        gtClust = new Clustering(clusters);

        System.out.println("Init creating macro clusters " + new Date());
        Clustering macro = moa.clusterers.KMeans.gaussianMeans(gtClust, clustering);
        System.out.println("End creating macro clusters " + new Date());

//        List<Cluster> macroClust = macro.getClustering();
//        for (int k = 0; k < macroClust.size(); k++) {
//            System.out.println("##########################");
//            System.out.println("Cluster " + k);
//            double[] centre = macroClust.get(k).getCenter();
////            for (int i = 0; i < centre.length; i++) {
////                if (centre[i] > 0.001) {
////                    System.out.println("Word: " + instance.attribute(i).name() + " value TD-IF: " + centre[i]);
////                }
////            }
//        }


        double[] total = new double[10000];
        double[] intersec = new double[10000];
        for (Cluster cluster : macro.getClustering()) {
            double[] centre = cluster.getCenter();
            for (int i = 0; i < centre.length; i++) {
                if (centre[i] != 0) {
                    if (total[i] != 0) {
                        intersec[i] = intersec[i] + centre[i];
                    }
                    total[i] += centre[i];
                }

            }
        }

        for (int i = 0; i < total.length; i++) {
            if (total[i] < 0.01)
                total[i] = 0;
        }


        for (int i = 0; i < intersec.length; i++) {
            if (intersec[i] == 0) {
                intersec[i] = 5;
            }
        }

        for (int i = 0; i < intersec.length; i++) {
            if (total[i] != 0) {
                System.out.println("Total index: " + i);
                System.out.println("Total value: " + total[i]);
            }
        }


        double min = NumberUtils.min(intersec);
        double max = NumberUtils.max(intersec);

        int minIdx1 = findIndex(intersec);
        intersec[minIdx1] = 55.0;

        int minIdx2 = findIndex(intersec);
        intersec[minIdx2] = 55.0;

        int minIdx3 = findIndex(intersec);
        intersec[minIdx3] = 55.0;

        int minIdx4 = findIndex(intersec);
        intersec[minIdx4] = 55.0;

        int minIdx5 = findIndex(intersec);
        intersec[minIdx5] = 55.0;

        System.out.println("Index1 " + instance.attribute(minIdx1).name());
        System.out.println("Index2 " + instance.attribute(minIdx2).name());
        System.out.println("Index3 " + instance.attribute(minIdx3).name());
        System.out.println("Index4 " + instance.attribute(minIdx4).name());
        System.out.println("Index5 " + instance.attribute(minIdx5).name());


        int minIdxTotal1 = findIndex(total);
        total[minIdxTotal1] = 55.0;

        int minIdxTotal2 = findIndex(total);
        total[minIdxTotal2] = 55.0;

        int minIdxTotal3 = findIndex(total);
        total[minIdxTotal3] = 55.0;

        int minIdxTotal4 = findIndex(total);
        total[minIdxTotal4] = 55.0;

        int minIdxTotal5 = findIndex(total);
        total[minIdxTotal5] = 55.0;

        System.out.println("Index total 1 " + instance.attribute(minIdxTotal1).name());
        System.out.println("Index total 2 " + instance.attribute(minIdxTotal2).name());
        System.out.println("Index total 3 " + instance.attribute(minIdxTotal3).name());
        System.out.println("Index total 4 " + instance.attribute(minIdxTotal4).name());
        System.out.println("Index total 5 " + instance.attribute(minIdxTotal5).name());


        return macro;
    }

    private void processMacro(Instance instance) throws IOException {
        Clustering gtClust = new Clustering(allPoints);
        Clustering macro = moa.clusterers.KMeans.gaussianMeans(gtClust, microAll);

        System.out.println("Macro clustering size: " + macro.size());

        List<Cluster> macroClust = macro.getClustering();
        int num = 555000;
        for (int k = 0; k < macroClust.size(); k++) {
            num++;
            writeToFile(num, "##########################");
            writeToFile(num, "Cluster " + k);


            double[] centre = macroClust.get(k).getCenter();
            System.out.println("INIT MACRO CLUST. Centre length: " + centre.length);
            for (int i = 0; i < centre.length; i++) {
                if (centre[i] != 0) {
                    writeToFile(num, instance.attribute(i).name() + "," + centre[i]);
                    System.out.println(instance.attribute(i).name() + "," + centre[i]);
                }
            }
        }

    }

    private Clustering processMicroClusters1(Clustering micro, ArrayList<DataPoint> points0, Instance instance) throws IOException {
        macroNumber++;
        Clustering gtClust = new Clustering(points0);
        System.out.println("Micro clustering size: " + micro.size());

        for (Cluster cluster : micro.getClustering()) {
            microAll.add(cluster);
        }
        allPoints.addAll(points0);
        while (allPoints.size() > decayHorizon)
            allPoints.removeFirst();


//        System.out.println("Init creating macro clusters " + new Date());
        macro = moa.clusterers.KMeans.gaussianMeans(gtClust, micro);
//        System.out.println("End creating macro clusters " + new Date());

        System.out.println("Macro clustering size: " + macro.size());

        List<Cluster> macroClust = macro.getClustering();
        for (int k = 0; k < macroClust.size(); k++) {
            writeToFile(macroNumber, "##########################");
            writeToFile(macroNumber, "Cluster " + k);


            double[] centre = macroClust.get(k).getCenter();
            for (int i = 0; i < centre.length; i++) {
                if (centre[i] != 0) {
                    writeToFile(macroNumber, instance.attribute(i).name() + "," + centre[i]);
                }
            }
        }


        return macro;
    }

    //add clusterer type add numAttributres
    private void writeToFile(int macroNumber, String text) throws IOException {
        String path = "/Users/raul/dev/uol/userInterest/cluster/ClusTree/61299563-XX/";
        String fileName = path + macroNumber + "-ClusTree-2000-clustering.csv";

        File file = new File(fileName);

        //if file doesnt exists, then create it
        if (!file.exists()) {
            file.createNewFile();
        }
        //true = append file
        FileWriter fileWritter = new FileWriter(file.getName(), true);
        PrintWriter printWriter = new PrintWriter(fileWritter);
        printWriter.println(text);
        printWriter.close();


    }

    private int findIndex(double[] array) {
        int index = 0;
        double min = NumberUtils.min(array);
        for (int i = 0; i < array.length; i++) {
            if (array[i] == min) {
                index = i;
            }
        }
        return index;
    }

    private double distance(double[] pointA, double[] pointB) {
        double distance = 0.0;
        for (int i = 0; i < pointA.length; i++) {
            double d = pointA[i] - pointB[i];
            distance += d * d;
        }
        return Math.sqrt(distance);
    }


}
