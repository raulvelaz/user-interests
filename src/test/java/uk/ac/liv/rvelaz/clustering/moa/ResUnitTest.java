package uk.ac.liv.rvelaz.clustering.moa;

import au.com.bytecode.opencsv.CSVReader;
import com.cybozu.labs.langdetect.Detector;
import com.cybozu.labs.langdetect.DetectorFactory;
import com.cybozu.labs.langdetect.LangDetectException;
import org.apache.commons.math.stat.descriptive.DescriptiveStatistics;
import org.junit.Ignore;
import org.junit.Test;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

/**
 * User: raul
 * Date: 21/12/2012
 * Time: 12:27
 */
public class ResUnitTest {
    int numOfDocs = 0;

    //news,world news,psychology,politics,research
    @Ignore
    @Test
    public void loadRes() throws IOException {
        Map<String, Double> map = new HashMap<String, Double>();

//        String file = "/Users/raul/dev/uol/userInterest/cluster/ClusTree/20471043/555001-ClusTree-2000-clustering.csv";
//        String file = "/Users/raul/dev/uol/userInterest/cluster/ClusTree/19117861/555001-ClusTree-2000-clustering.csv";
//        String file = "/Users/raul/dev/uol/userInterest/cluster/ClusTree/75655723/555001-ClusTree-2000-clustering.csv";
        String file = "/Users/raul/dev/uol/userInterest/cluster/ClusTree/61299563-XX/555001-ClusTree-2000-clustering.csv";
//        String file = "/Users/raul/dev/uol/userInterest/cluster/ClusTree/367574327/555001-ClusTree-2000-clustering.csv";
        CSVReader reader = new CSVReader(new FileReader(file), ',', '\n', 2);
        String[] nextLine;
        DescriptiveStatistics stats = new DescriptiveStatistics();

        while ((nextLine = reader.readNext()) != null) {
            String attribute = nextLine[0];
            double val = Double.parseDouble(nextLine[1]);
            stats.addValue(val);
            map.put(attribute, val);
        }

        // Get entries and sort them.
        List<Map.Entry<String, Double>> entries = new ArrayList<Map.Entry<String, Double>>(map.entrySet());
        Collections.sort(entries, new Comparator<Map.Entry<String, Double>>() {
            public int compare(Map.Entry<String, Double> e1, Map.Entry<String, Double> e2) {
                return e2.getValue().compareTo(e1.getValue());
            }
        });

// Put entries back in an ordered map.
        Map<String, Double> orderedMap = new LinkedHashMap<String, Double>();
        for (Map.Entry<String, Double> entry : entries) {
            orderedMap.put(entry.getKey(), entry.getValue());
        }

//        System.out.println(orderedMap);


        double mean = stats.getMean();
        double std = stats.getStandardDeviation();
        double min = stats.getMin();
        double max = stats.getMax();

        System.out.println("mean: " + mean);
        System.out.println("std: " + std);
        System.out.println("min: " + min);
        System.out.println("max: " + max);

        int cont = 1;
        for (String attribute : orderedMap.keySet()) {
//            if (map.get(attribute) <= 5E-3 && map.get(attribute) > 2E-3) {
            if (orderedMap.get(attribute) <= Math.abs(mean - std * 2) * 5 && orderedMap.get(attribute) > mean * 5) {
//            if(map.get(attribute) > 0.001 ){
                System.out.println(cont + ". " + attribute + " " + orderedMap.get(attribute));
                cont++;
            }
        }

    }
    @Ignore
    @Test
    public void clusterRes() throws IOException, LangDetectException {
//        String path = "/Users/raul/dev/uol/userInterest/cluster/02/";
//        String path = "/Users/raul/dev/uol/userInterest/cluster/ClusTree/19117861/";
//        String path = "/Users/raul/dev/uol/userInterest/cluster/ClusTree/84545107/";
//        String path = "/Users/raul/dev/uol/userInterest/cluster/ClusTree/84545107/";
        String path = "/Users/raul/dev/uol/userInterest/cluster/ClusTree/769289/";

        //Raskoul", "Talk of the hour","Politics","News", "Latest technology","Photography"
//        String path = "/Users/raul/dev/uol/userInterest/cluster/ClusTree/75655723/";
//        String path = "/Users/raul/dev/uol/userInterest/cluster/ClusTree/2000/";
//        String path = "/Users/raul/dev/uol/userInterest/cluster/clustream/2000";
        File f = new File(path);
        File[] files = f.listFiles();
//        String file1 = "/Users/raul/dev/uol/userInterest/cluster/ClusTree/2000/90-clustering.csv";
//        String file1 = "/Users/raul/dev/uol/userInterest/cluster/ClusTree/2000/178-clustering.csv";

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < files.length; i++) {
            String text = readFromFile(files[i].getAbsolutePath());
            sb.append(text);
            sb.append(" ");
        }

        Map<String, Integer> map = tf(sb.toString());


        double[] vals = new double[map.keySet().size()];


        int i = 0;
        DescriptiveStatistics stats = new DescriptiveStatistics();
        for (String s : map.keySet()) {
            stats.addValue(map.get(s));
            i++;
        }

        double mean = stats.getMean();
        double std = stats.getStandardDeviation();

        double max = stats.getMax();
        double min = stats.getMin();

        System.out.println("Max " + max);
        System.out.println("Min " + min);
        System.out.println("Mean " + mean);
        System.out.println("Std " + std);

        DetectorFactory.loadProfile(getClass().getResource("/profiles").getFile());
        Detector detector = DetectorFactory.create();
        //"politics", "world news", "news", "psychology", "research"
        //celebrities,comics,musicians
        HashMap<Integer, List<String>> clust = new HashMap<Integer, List<String>>();

        for (String s : map.keySet()) {
//            if (map.get(s) <= Math.floor(mean) + 1 )
//            if (map.get(s) < 20 )
//                System.out.println(s + ":::" + map.get(s));
            if (map.get(s) > (mean - std) && map.get(s) < (mean)) {
                if (clust.get(map.get(s)) == null) {
                    List<String> values = new ArrayList<String>();
                    values.add(s);
                    clust.put(map.get(s), values);
                } else {
                    List<String> values = clust.get(map.get(s));
                    values.add(s);
                    clust.put(map.get(s), values);
                }
//                System.out.println(s + " " + map.get(s));
            }
        }

        for (Integer key : clust.keySet()) {
            System.out.println(key + " " + clust.get(key));
        }


//        for(String s : map.keySet()){
//            if(map.get(s) >80)
//                System.out.println(s + " " + map.get(s));
//        }

    }


    private String readFromFile(String file) throws IOException {
        StringBuilder sb = new StringBuilder();
        CSVReader reader = new CSVReader(new FileReader(file), ',', '\n', 2);
        String[] nextLine;
        while ((nextLine = reader.readNext()) != null) {
            String attribute = nextLine[0];
            if (nextLine.length == 2) {
                double val = Double.parseDouble(nextLine[1]);
//                if (val <= 5E-3 && val > 2E-3) {
                sb.append(attribute);
                sb.append(" ");
//                }
            }
        }
        return sb.toString().trim();
    }


    private Map<String, Integer> tf(String text) {
        numOfDocs++;    //New document, count it.

        String[] tokens = text.split(" ");    //Get the individual tokens.
        double docSize = tokens.length;         //Number of tokens in the document.
        Map<String, Integer> tokensInDoc = new HashMap<String, Integer>();

        for (String token : tokens) {
            //For each token in the document
            if (!token.equals(" ") && !token.equals("")) {
                Integer freq = tokensInDoc.get(token.toLowerCase()); //Compute freq for each token
                tokensInDoc.put(token.toLowerCase(), (freq == null) ? 1 : freq + 1);
            }
        }
        return tokensInDoc;
    }
}
